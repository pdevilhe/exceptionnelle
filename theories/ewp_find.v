From iris.proofmode      Require Import base tactics classes.
From iris.base_logic.lib Require Import iprop.
From exceptionnelle      Require Export notation exn_weakestpre.

Section ewp_find.

Context `{StateInterp Σ}.
Context (iter : val).
Context (isList : val → list val → iProp Σ).

Context (iter_spec :
 (∀ (* The loop invariant: *)
      (I : list val → iProp Σ)
    (* The state in case of an error: *)
      (J : list val → val → iProp Σ)
    (l : val ) (xs : list val) (f : val),

      (* The specification for the function being iterated: *)
      (∀ ys y, ⌜ (ys ++ [y]) `prefix_of` xs ⌝ →
         □ (I ys -∗ EWP f y {{ λ _, I (ys ++ [y]) }} {{ λ y, J ys y }}))

    -∗

    (* Finally, the specification for the 'iter' function: *)
    □ (isList l xs -∗ I [] -∗
       EWP iter f l {{ λ _, isList l xs ∗       I xs   }}
                    {{ λ y, isList l xs ∗ ∃ ys, J ys y }})
)%I).

Context (pred : val).
Context (P : val → bool).
Context (pred_spec :
  (∀ (x : val), EWP pred x {{ λ b, ⌜ b = #(P x) ⌝ }} {{ λ _, False}})%I).

Definition find : val := λ: "pred" "xs",
  try:
    iter (λ: "x", if: "pred" "x" then raise "x" else #()) "xs";; NONE
  with
    "x" => SOME "x"
  end.

Lemma find_spec l xs :
  (□ (isList l xs -∗
      EWP find pred l
      {{ λ v, isList l xs ∗ match v with
        | NONEV   => ⌜ Forall (not ∘ P) xs ⌝
        | SOMEV y => ∃ ys, ⌜ (ys ++ [y]) `prefix_of` xs ⌝ ∗
                        ⌜ is_true (P y) ⌝ ∗ 
                     ⌜ Forall (not ∘ P) ys ⌝
        | _       => False
        end
      }}
      {{ λ _, False }}))%I.
Proof.
  iModIntro. iIntros "Hl".
  unfold find.
  iApply (Ectxi_ewp_bind (AppLCtx _)). reflexivity.
  iApply ewp_pure_step. apply pure_prim_step_beta. simpl.
  iApply ewp_pure_step. apply pure_prim_step_rec.
  iApply ewp_value.
  iApply ewp_pure_step. apply pure_prim_step_beta. simpl.
  iApply ewp_try_with.
  iApply (Ectxi_ewp_bind (AppRCtx _)). reflexivity.
  iApply (ewp_bind [(AppLCtx _); (AppRCtx _)]). reflexivity.
  iApply ewp_pure_step. apply pure_prim_step_rec.
  iApply ewp_value. simpl.
  iApply (ewp_mono' with "[Hl]").
  - iApply (iter_spec $! (λ ys,   ⌜ Forall (not ∘ P) ys ⌝)%I
                         (λ ys y, ⌜ (ys ++ [y]) `prefix_of` xs ⌝ ∗
                                  ⌜ is_true (P y) ⌝ ∗ 
                                  ⌜ Forall (not ∘ P) ys ⌝)%I with "[] Hl");
    [| iPureIntro; by apply Forall_nil ].
    iIntros (ys y xs_prefix). iModIntro. iIntros (Forall_not).
    iApply ewp_pure_step. apply pure_prim_step_beta. simpl.
    iApply (Ectxi_ewp_bind (IfCtx _ _)). reflexivity.
    iPoseProof (pred_spec $! y) as "#pred_spec".
    iApply (ewp_mono' with "pred_spec"); [| by iIntros (v ?) ].
    iIntros (b ->). iModIntro. simpl.
    case_eq (P y); [ intro P_true | intro P_false ].
    + iApply ewp_pure_step. apply pure_prim_step_if_true.
      iApply ewp_pure_step. apply pure_prim_step_raise.
      iApply ewp_raise; by auto.
    + iApply ewp_pure_step. apply pure_prim_step_if_false.
      iApply ewp_value. iPureIntro.
      apply Forall_app; split; [| apply Forall_cons; split; [| apply Forall_nil ]]; auto.
      simpl; rewrite P_false. by intro.
  - iIntros (v) "[Hl %]". rename H0 into Forall_not. iModIntro.
    iApply (Ectxi_ewp_bind (AppLCtx _)). reflexivity.
    iApply ewp_pure_step. apply pure_prim_step_rec.
    iApply ewp_value.
    iApply ewp_pure_step. apply pure_prim_step_beta. simpl.
    iApply ewp_pure_step. apply pure_prim_step_InjL.
    iApply ewp_value.
    by iFrame.
  - iIntros (y) "[Hl %]". rename H0 into Post. iModIntro.
    iApply (Ectxi_ewp_bind (AppLCtx _)). reflexivity.
    iApply ewp_pure_step. apply pure_prim_step_rec.
    iApply ewp_value.
    iApply ewp_pure_step. apply pure_prim_step_beta. simpl.
    iApply ewp_pure_step. apply pure_prim_step_InjR.
    iApply ewp_value.
    by iFrame.
Qed.

End ewp_find.
