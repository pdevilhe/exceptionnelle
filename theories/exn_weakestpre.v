From iris.proofmode      Require Import base tactics classes.
From iris.base_logic.lib Require Import iprop wsat.
From exceptionnelle      Require Import lang.

(** Exceptional Weakest Precondition.

  In the theory lang.v, we have proved that exn_lang is a language in the Iris
  sense. One advantage of proving this result is that we obtain a program logic
  for free. Concretely, we get a notion of weakest precondition (together with a
  number of theorems about its proof theory) that can be applied to exn_lang
  programs. Unfortunately, this notion of weakest precondition is not suited for
  the verification of programs that tamper with the evaluation context. To
  convince ourselves, just remember that an adequacy theorem is proven for this
  notion and that the program [raise v] does not loop nor reduces to a value.
  Hence, no specification of the form [WP raise v {{ Φ }}] can possibly be
  proven.

  A well known solution for this problem is to add a second postcondition whose
  purpose is to describe the state at the moment that an exception is raised.
  Thus, we arrive at a notion of WP, dubbed exceptional WP and written as
  [EWP e {{ Φ₁ }} {{ Φ₂ }}]. Now, the adequacy theorem can be adjusted to assure
  that the program [e] either loops, or it reduces to a value [v] satisfying
  [Φ₁] or it raises an exception [raise v] during its execution, with [v]
  satisfying [Φ₂].

  Here, we provide the formal definition of exceptional weakest precondition as
  the fixed point of [ewp_pre] and we prove some lemmas about its proof theory.
  We list some of the most interesting bellow.


  Contents:

  - Frame rule: [ewp_frame_l] and [ewp_frame_r].

      [EWP e {{ Φ₁ }} {{ Φ₂ }} ∗ R ⊢ EWP e {{ λ v,
                Φ₁ v           ∗ R }}      {{ λ v,
                         Φ₂ v  ∗ R }}].

  - Bind rule: [ewp_bind'], [ewp_bind] and [Ectxi_ewp_bind].

      [EWP e {{ λ v,
       EWP fill K v {{ Φ₁ }} {{ Φ₂ }} }} {{ Φ₂ }} ⊢
       EWP fill K e {{ Φ₁ }} {{ Φ₂ }}].

  - Try-with rule: [ewp_try_with'] and [ewp_try_with].

      [EWP e₁ {{ Φ₁ }} {{ λ v, EWP e₂ v {{ Φ₁ }} {{ Φ₂ }} }} ⊢
       EWP try e₁ with e₂ end {{ Φ₁ }} {{ Φ₂ }}].

*)

Class StateInterp Σ := state_interp : state → iProp Σ.

Definition ewp_pre `{StateInterp Σ} :
  (expr -d> (val -d> iPropO Σ) -d> (val -d> iPropO Σ) -d> iPropO Σ) →
  (expr -d> (val -d> iPropO Σ) -d> (val -d> iPropO Σ) -d> iPropO Σ)
  := λ ewp e₁ Φ₁ Φ₂,
  match to_val   e₁ with Some v => |==> Φ₁ v | _ =>
  match to_error e₁ with Some v => |==> Φ₂ v | _ =>
    ∀ σ₁,
     state_interp σ₁ ==∗
       ⌜ reducible e₁ σ₁ ⌝ ∗ 
       ▷ ∀ e₂ σ₂ e₃, ⌜ prim_step e₁ σ₁ e₂ σ₂ e₃ ⌝ ==∗
         state_interp σ₂ ∗ ewp e₂ Φ₁ Φ₂ ∗
         from_option (λ e, ewp e (λ _, True) (λ _, False)) True e₃
  end
  end%I.

Local Instance ewp_pre_contractive `{StateInterp Σ} : Contractive (ewp_pre).
Proof.
  rewrite /ewp_pre /from_option=> n ewp ewp' Hwp e Φ₁ Φ₂.
  repeat (f_contractive || f_equiv); try apply Hwp.
Qed.

Definition ewp_def `{StateInterp Σ} :
  expr → (val → iProp Σ) → (val → iProp Σ) → iProp Σ := fixpoint (ewp_pre).
Definition ewp_aux `{StateInterp Σ} : seal (@ewp_def _ _). by eexists. Qed.
Definition ewp_eq `{StateInterp Σ} := ewp_aux.(seal_eq).

Notation "'EWP' e {{ Φ₁ } } {{ Φ₂ } }" := (ewp_def e%E Φ₁ Φ₂)
  (at level 20, e, Φ₁, Φ₂ at level 200) : bi_scope.

Section ewp.
Context `{StateInterp Σ}.

Lemma ewp_unfold e Φ₁ Φ₂ :
  EWP e {{ Φ₁ }} {{ Φ₂ }} ⊣⊢ ewp_pre (ewp_def) e Φ₁ Φ₂.
Proof. apply (fixpoint_unfold ewp_pre). Qed.

Global Instance ewp_ne e n :
  Proper (pointwise_relation _ (dist n) ==>
          pointwise_relation _ (dist n) ==>
                                dist n)
         (ewp_def e).
Proof.
  revert e. induction (lt_wf n) as [n _ IH]=> e Φ₁ Ψ₁ HΦ₁ Φ₂ Ψ₂ HΦ₂.
  rewrite !ewp_unfold /ewp_pre /from_option.
  do 18 (f_contractive || f_equiv);
  try  f_contractive;
  try (apply IH; first lia;
       intros v; eapply dist_le; eauto with lia).
Qed.

Global Instance ewp_proper e :
  Proper (pointwise_relation _ (≡) ==>
          pointwise_relation _ (≡) ==>
                               (≡))
         (ewp_def e).
Proof.
  by intros Φ₁ Ψ₁ ? Φ₂ Ψ₂ ?; apply equiv_dist=>n; apply ewp_ne=>v; apply equiv_dist.
Qed.

Global Instance ewp_contractive e n :
  to_val   e = None →
  to_error e = None →
  Proper (pointwise_relation _ (dist_later n) ==> 
          pointwise_relation _ (dist_later n) ==> 
                                      dist n)
         (ewp_def e).
Proof.
  intros ?? Φ₁ Ψ₁ HΦ₁ Φ₂ Ψ₂ HΦ₂.
  rewrite !ewp_unfold /ewp_pre H0 H1.
  by repeat (f_contractive || f_equiv).
Qed.

Lemma ewp_value Φ₁ Φ₂ v : Φ₁ v ⊢ EWP of_val v {{ Φ₁ }} {{ Φ₂ }}.
Proof. iIntros "HΦ". by rewrite ewp_unfold /ewp_pre. Qed.
Lemma ewp_value_inv Φ₁ Φ₂ v : EWP of_val v {{ Φ₁ }} {{ Φ₂ }} ==∗ Φ₁ v.
Proof. by rewrite ewp_unfold /ewp_pre to_of_val. Qed.

Lemma ewp_raise Φ₁ Φ₂ v : Φ₂ v ⊢ EWP RaiseV v {{ Φ₁ }} {{ Φ₂ }}.
Proof. iIntros "HΦ". by rewrite ewp_unfold /ewp_pre. Qed.
Lemma ewp_raise_inv Φ₁ Φ₂ v : EWP RaiseV v {{ Φ₁ }} {{ Φ₂ }} ==∗ Φ₂ v.
Proof. by rewrite ewp_unfold /ewp_pre to_of_error. Qed.

Lemma ewp_strong_mono R Φ₁ Φ₂ Ψ₁ Ψ₂ e :
  EWP e {{ Φ₁ }} {{ Φ₂ }} -∗ R -∗
      (∀ v, Φ₁ v -∗ R ==∗ Ψ₁ v) -∗
      (∀ v, Φ₂ v -∗ R ==∗ Ψ₂ v) -∗
  EWP e {{ Ψ₁ }} {{ Ψ₂ }}.
Proof.
  iLöb as "IH" forall (e).
  rewrite !ewp_unfold /ewp_pre.
  destruct (to_val e);[| destruct (to_error e)].
  - iIntros "H HR HΦ₁ _"; iMod "H"; by iApply ("HΦ₁" with "H").
  - iIntros "H HR _ HΦ₂"; iMod "H"; by iApply ("HΦ₂" with "H").
  - iIntros "H HR HΦ₁ HΦ₂" (σ) "Hs".
    iMod ("H" $! σ with "Hs") as "[Hred Hewp]"; iFrame.
    iModIntro. iNext. iIntros (e₂ σ₂ e₃) "Hstep".
    iMod ("Hewp" $! e₂ σ₂ with "Hstep") as "[$ [Hewp' $]]".
    by iApply ("IH" $! e₂ with "[Hewp'] [HR] [HΦ₁] [HΦ₂]").
Qed.

Lemma ewp_mono' Φ₁ Φ₂ Ψ₁ Ψ₂ e :
  EWP e {{ Φ₁ }} {{ Φ₂ }} -∗
      (∀ v, Φ₁ v ==∗ Ψ₁ v) -∗
      (∀ v, Φ₂ v ==∗ Ψ₂ v) -∗
  EWP e {{ Ψ₁ }} {{ Ψ₂ }}.
Proof.
  iIntros "Hewp HΦ₁ HΦ₂".
  iApply (ewp_strong_mono (True%I) Φ₁ Φ₂ with "Hewp [] [HΦ₁] [HΦ₂]"); auto.
  - iIntros (?) "H _"; by iApply "HΦ₁".
  - iIntros (?) "H _"; by iApply "HΦ₂".
Qed.

Lemma ewp_mono Φ₁ Φ₂ Ψ₁ Ψ₂ e :
  (∀ v, Φ₁ v -∗ Ψ₁ v) →
  (∀ v, Φ₂ v -∗ Ψ₂ v) →
  EWP e {{ Φ₁ }} {{ Φ₂ }} ⊢
  EWP e {{ Ψ₁ }} {{ Ψ₂ }}.
Proof.
  iIntros (HΦ₁ HΦ₂) "Hewp"; iApply (ewp_strong_mono True%I with "Hewp"); auto.
  - iIntros (v) "? _"; by iApply HΦ₁.
  - iIntros (v) "? _"; by iApply HΦ₂.
Qed.

Lemma ewp_frame_r Φ₁ Φ₂ R e :
  EWP e {{ Φ₁ }} {{ Φ₂ }} ∗ R ⊢ EWP e {{ λ v, Φ₁ v ∗ R }} {{ λ v, Φ₂ v ∗ R }}.
Proof. iIntros "[H HR]". iApply (ewp_strong_mono R with "H HR"); auto with iFrame. Qed.

Lemma ewp_frame_l Φ₁ Φ₂ R e :
  R ∗ EWP e {{ Φ₁ }} {{ Φ₂ }} ⊢ EWP e {{ λ v, R ∗ Φ₁ v }} {{ λ v, R ∗ Φ₂ v }}.
Proof. iIntros "[HR H]". iApply (ewp_strong_mono R with "H HR"); auto with iFrame. Qed.

Lemma fupd_ewp e Φ₁ Φ₂ : (|==> EWP e {{ Φ₁ }} {{ Φ₂ }}) ⊢ EWP e {{ Φ₁ }} {{ Φ₂ }}.
Proof.
  rewrite ewp_unfold /ewp_pre. iIntros "H".
    destruct (to_val e)   as [v|] eqn:?;
  [|destruct (to_error e) as [v|] eqn:?].
  - by iMod "H".
  - by iMod "H".
  - iIntros (σ1) "Hσ1". iMod "H". by iApply "H".
Qed.

Lemma ewp_fupd e Φ₁ Φ₂ :
  EWP e {{ λ v, |==> Φ₁ v }} {{ λ v, |==> Φ₂ v }} ⊢ EWP e {{ Φ₁ }} {{ Φ₂ }}.
Proof. iIntros "H". iApply (ewp_strong_mono True%I with "H"); auto. Qed.

Lemma ewp_value_fupd Φ₁ Φ₂ v : (|==> Φ₁ v) ⊢ EWP of_val v {{ Φ₁ }} {{ Φ₂ }}.
Proof. intros. by rewrite -ewp_fupd -ewp_value. Qed.

Lemma ewp_raise_fupd Φ₁ Φ₂ v : (|==> Φ₂ v) ⊢ EWP of_error v {{ Φ₁ }} {{ Φ₂ }}.
Proof. intros. by rewrite -ewp_fupd -ewp_raise. Qed.


(** Pure steps. *)

Record pure_prim_step (e1 e2 : expr) := {
  pure_prim_step_safe σ1 : reducible e1 σ1;
  pure_prim_step_det σ1 e2' σ2 e3 :
    prim_step e1 σ1 e2' σ2 e3 → σ2 = σ1 ∧ e2' = e2 ∧ e3 = None
}.

Lemma val_not_pure v e : ¬ pure_prim_step (Val v) e.
Proof.
  intros H0. specialize (pure_prim_step_safe (Val v) e H0 {|heap:=∅|}) as H1.
  by specialize (reducible_not_val (Val v) {|heap:=∅|} H1).
Qed.

Lemma val_not_pure' v e e' : to_val e = Some v → ¬ pure_prim_step e e'.
Proof. intro H1; rewrite <- (of_to_val _ _ H1). by apply val_not_pure. Qed.

Lemma error_not_pure v e : ¬ pure_prim_step (of_error v) e.
Proof.
  intros H0. specialize (pure_prim_step_safe _ _ H0 {|heap:=∅|}).
  specialize (error_irreducible v {|heap:=∅|}).
  by rewrite <-not_reducible.
Qed.

Lemma error_not_pure' v e e' : to_error e = Some v → ¬ pure_prim_step e e'.
Proof. intro H1; rewrite <- (of_to_error _ _ H1). by apply error_not_pure. Qed.

Lemma ewp_pure_step' e e' Φ₁ Φ₂ :
  pure_prim_step e e' → 
    ▷ EWP e' {{ Φ₁ }} {{ Φ₂ }} ⊢ EWP e {{ Φ₁ }} {{ Φ₂ }}.
Proof.
  intros Hstep.
  rewrite !ewp_unfold /ewp_pre.
    destruct (to_val   e) as [v|] eqn:?;
  [|destruct (to_error e) as [v|] eqn:?].
  - by specialize   (val_not_pure' _ _ e' Heqo).
  - by specialize (error_not_pure' _ _ e' Heqo0).
  -   destruct (to_val   e') as [v'|] eqn:?;
    [|destruct (to_error e') as [v'|] eqn:?];
    iIntros "Hv'" (σ₁) "Hs"; iModIntro;
    iSplitR; try (iPureIntro; by apply Hstep);
    iNext; iIntros (e₂ σ₂ e₃) "%"; rename a into Hpstep;
    destruct (pure_prim_step_det _ _ Hstep _ _ _ _ Hpstep) as [-> [-> ->]];
    iModIntro; iFrame; rewrite /from_option; iSplit; try done.
    + rewrite -(of_to_val   _ _ Heqo1). by iApply ewp_value_fupd.
    + rewrite -(of_to_error _ _ Heqo2). by iApply ewp_raise_fupd.
    + rewrite !ewp_unfold /ewp_pre Heqo1 Heqo2. done.
Qed.

Lemma ewp_pure_step e e' Φ₁ Φ₂ :
  pure_prim_step e e' → 
      EWP e' {{ Φ₁ }} {{ Φ₂ }} ⊢ EWP e {{ Φ₁ }} {{ Φ₂ }}.
Proof. iIntros "% Hwp". iApply (ewp_pure_step' with "[Hwp]"); by auto. Qed.

Lemma pure_prim_step_beta f x e v :
  pure_prim_step ((App (Val $ RecV f x e) (Val v)))
                 (subst' x v (subst' f (RecV f x e) e)).
Proof.
  constructor.
  - unfold reducible; simpl. unfold prim_step'; simpl.
    intros ?. exists [], (subst' x v (subst' f (RecV f x e) e)), σ1, [].
    apply (Ectx_prim_step _ _ _ _ _ [] ((App (Val $ RecV f x e) (Val v)))
                                      (subst' x v (subst' f (RecV f x e) e)));
    eauto. by apply BetaS.
  - intros ???. inversion 1.
    destruct K as [|Ki K].
    + simpl in H1, H2. simplify_eq.
      inversion H3. by simplify_eq.
    + destruct Ki; simpl in H1, H2; try naive_solver.
      * inversion H1.
        specialize (fill_val' _ _ _ (eq_sym H5)) as ?.
        simplify_eq. by inversion H3.
      * inversion H1.
        specialize (fill_val' _ _ _ (eq_sym H6)) as ?.
        simplify_eq. by inversion H3.
Qed.

Lemma pure_prim_step_rec f x e :
  pure_prim_step (Rec f x e) (Val $ RecV f x e).
Proof.
  constructor.
  - unfold reducible; simpl. unfold prim_step'; simpl.
    intros ?. exists [], (Val $ RecV f x e), σ1, [].
    apply (Ectx_prim_step _ _ _ _ _ [] (Rec f x e) (Val $ RecV f x e));
    eauto. by apply RecS.
  - intros ???. inversion 1.
    destruct K as [|Ki K].
    + simpl in H1, H2. simplify_eq.
      inversion H3. by simplify_eq.
    + destruct Ki; simpl in H1, H2; by naive_solver.
Qed.

Lemma pure_prim_step_InjL v :
  pure_prim_step (InjL $ Val v) (Val $ InjLV v).
Proof.
  constructor.
  - unfold reducible; simpl. unfold prim_step'; simpl.
    intros ?. exists [], (Val $ InjLV v), σ1, [].
    apply (Ectx_prim_step _ _ _ _ _ [] (InjL $ Val v) (Val $ InjLV v));
    eauto. by apply InjLS.
  - intros ???. inversion 1.
    destruct K as [|Ki K].
    + simpl in H1, H2. simplify_eq.
      inversion H3. by simplify_eq.
    + destruct Ki; simpl in H1, H2; try naive_solver.
      inversion H1.
      specialize (fill_val' _ _ _ (eq_sym H5)) as ?.
      simplify_eq. by inversion H3.
Qed.

Lemma pure_prim_step_InjR v :
  pure_prim_step (InjR $ Val v) (Val $ InjRV v).
Proof.
  constructor.
  - unfold reducible; simpl. unfold prim_step'; simpl.
    intros ?. exists [], (Val $ InjRV v), σ1, [].
    apply (Ectx_prim_step _ _ _ _ _ [] (InjR $ Val v) (Val $ InjRV v));
    eauto. by apply InjRS.
  - intros ???. inversion 1.
    destruct K as [|Ki K].
    + simpl in H1, H2. simplify_eq.
      inversion H3. by simplify_eq.
    + destruct Ki; simpl in H1, H2; try naive_solver.
      inversion H1.
      specialize (fill_val' _ _ _ (eq_sym H5)) as ?.
      simplify_eq. by inversion H3.
Qed.

Lemma pure_prim_step_if e1 e2 b :
  pure_prim_step (If (Val $ LitV $ LitBool b) e1 e2) (if b then e1 else e2).
Proof.
  constructor.
  - unfold reducible; simpl. unfold prim_step'; simpl.
    intros ?. exists [], (if b then e1 else e2), σ1, [].
    apply (Ectx_prim_step _ _ _ _ _ [] (If (Val $ LitV $ LitBool b) e1 e2)
                                       (if b then e1 else e2));
    eauto.
    destruct b; [ by apply IfTrueS | by apply IfFalseS ].
  - intros ???. inversion 1.
    destruct K as [|Ki K].
    + simpl in H1, H2. simplify_eq.
      destruct b; inversion H3; by simplify_eq.
    + destruct Ki; simpl in H1, H2; try naive_solver.
      inversion H1.
      specialize (fill_val' _ _ _ (eq_sym H5)) as ?.
      simplify_eq. by inversion H3.
Qed.

Lemma pure_prim_step_if_true e1 e2 :
  pure_prim_step (If (Val $ LitV $ LitBool true) e1 e2) e1.
Proof. by apply pure_prim_step_if. Qed.

Lemma pure_prim_step_if_false e1 e2 :
  pure_prim_step (If (Val $ LitV $ LitBool false) e1 e2) e2.
Proof. by apply pure_prim_step_if. Qed.

Lemma pure_prim_step_pair v1 v2 :
  pure_prim_step (Pair (Val v1) (Val v2)) (Val $ PairV v1 v2).
Proof.
  constructor.
  - unfold reducible; simpl. unfold prim_step'; simpl.
    intros ?. exists [], (Val $ PairV v1 v2), σ1, [].
    apply (Ectx_prim_step _ _ _ _ _ [] (Pair (Val v1) (Val v2)) (Val $ PairV v1 v2)); eauto.
    by apply PairS.
  - intros ???. inversion 1.
    destruct K as [|Ki K].
    + simpl in H1, H2. simplify_eq.
      inversion H3; by simplify_eq.
    + destruct Ki; simpl in H1, H2; try naive_solver;
      inversion H1;
      try specialize (fill_val' _ _ _ (eq_sym H5)) as ?;
      try specialize (fill_val' _ _ _ (eq_sym H6)) as ?;
      simplify_eq; by inversion H3.
Qed.

Lemma pure_prim_step_raise v :
  pure_prim_step (Raise $ Val v) (RaiseV v).
Proof.
  constructor.
  - unfold reducible; simpl. unfold prim_step'; simpl.
    intros ?. exists [], (RaiseV v), σ1, [].
    apply (Ectx_prim_step _ _ _ _ _ [] (Raise $ Val v) (RaiseV v)); eauto.
    by apply RaiseS.
  - intros ???. inversion 1.
    destruct K as [|Ki K].
    + simpl in H1, H2. simplify_eq.
      inversion H3; by simplify_eq.
    + destruct Ki; simpl in H1, H2; try naive_solver;
      inversion H1;
      try specialize (fill_val' _ _ _ (eq_sym H5)) as ?;
      try specialize (fill_val' _ _ _ (eq_sym H6)) as ?;
      simplify_eq; by inversion H3.
Qed.

Lemma TryWithVal_det v e₁ e₂ e₃ σ₁ σ₂ :
  prim_step (TryWith (Val v) e₁) σ₁ e₂ σ₂ e₃ →
  e₂ = Val v ∧ σ₁ = σ₂ ∧ e₃ = None.
Proof.
  inversion 1. destruct K as [|Ki K].
  - simpl in H1, H2; simplify_eq. by inversion H3.
  - destruct Ki; try naive_solver. simpl in H1, H2.
    destruct K as [| Kj K]; [| simpl; destruct Kj; naive_solver].
    simplify_eq. by specialize (val_head_stuck _ _ _ _ _ H3).
Qed.

Lemma pure_prim_step_try_with_val v e₂ :
  pure_prim_step (TryWith (Val v) e₂) (Val v).
Proof.
  constructor.
  - unfold reducible; simpl. unfold prim_step'; simpl.
    intros ?. exists [], (Val v), σ1, [].
    apply (Ectx_prim_step _ _ _ _ _ [] (TryWith (Val v) e₂) (Val v)); eauto.
    by apply TryWithValS.
  - intros ???? H1. by destruct (TryWithVal_det _ _ _ _ _ _ H1) as [-> [-> ->]].
Qed.

Lemma TryWithRaise_det v e₁ e₂ e₃ σ₁ σ₂ :
  prim_step (TryWith (RaiseV v) e₁) σ₁ e₂ σ₂ e₃ →
  e₂ = App e₁ (Val v) ∧ σ₁ = σ₂ ∧ e₃ = None.
Proof.
  inversion 1. destruct K as [|Ki K].
  - simpl in H1, H2; simplify_eq. by inversion H3.
  - destruct Ki; try naive_solver.
    simpl in H1, H2; simplify_eq.
    destruct K as [|Kj K].
    + simpl in H1; simplify_eq. by inversion H3.
    + destruct K as [|Kl K]; destruct Kj; try destruct Kl; by naive_solver.
Qed.

Lemma pure_prim_step_try_with_error v e₂ :
  pure_prim_step (TryWith (of_error v) e₂) (App e₂ (Val v)).
Proof.
  constructor.
  - unfold reducible; simpl. unfold prim_step'; simpl.
    intros ?. exists [], (App e₂ (Val v)), σ1, [].
    apply (Ectx_prim_step _ _ _ _ _ []
          (TryWith (of_error v) e₂)
          (App e₂ (Val v))); eauto.
    by apply TryWithRaiseS.
  - intros ???? H1. by destruct (TryWithRaise_det _ _ _ _ _ _ H1) as [-> [-> ->]].
Qed.

Lemma TryWith_not_neutral e2 : ¬ NeutralEctxi (TryWithCtx e2).
Proof.
  intros ?.
  cut (head_step (TryWith (RaiseV (LitV LitUnit)) e2) {|heap:=∅|}
                          (RaiseV (LitV LitUnit))     {|heap:=∅|} None).
  - by inversion 1.
  - by apply H0.
Qed.

Lemma NeutralEctxi_det `{NeutralEctxi Ki} v e₂ e₃ σ₁ σ₂ :
  prim_step (fill_item Ki (of_error v)) σ₁ e₂ σ₂ e₃ →
  e₂ = of_error v ∧ σ₁ = σ₂ ∧ e₃ = None.
Proof.
  inversion 1. destruct K as [|Kj K].
  - simpl in H2, H3. simplify_eq.
    destruct Ki; simpl in H4; inversion H4; try naive_solver.
    by specialize (TryWith_not_neutral e2).
  - simpl in H2. simplify_eq.
    cut ((fill K e1' = of_error v) ∨ (∃ v', fill K e1' = Val v')).
    + destruct K as [|Kl K]; simpl.
      * case; [intros -> | destruct 1 as [v' ->]]; by inversion H4.
      * case; [| destruct 1 as [v' H5]; destruct Kl; by naive_solver].
        destruct K as [|Km K]; [| destruct Kl, Km; by naive_solver].
        simpl. destruct Kl; by naive_solver.
    + destruct Ki, Kj; simpl in H2; by naive_solver.
Qed.

Lemma pure_prim_step_neutral_ectxi_error `{NeutralEctxi Ki} v :
  pure_prim_step (fill_item Ki (of_error v)) (of_error v).
Proof.
  constructor.
  - by apply reducible_fill_item_raiseV.
  - intros ???? H1. by destruct (NeutralEctxi_det _ _ _ _ _ H1) as [-> [-> ->]].
Qed.

Lemma Ectxi_prim_step_inv Ki `{NeutralEctxi Ki} e e₂ e₃ σ₁ σ₂ :
    to_val e = None →
  to_error e = None →
  prim_step (fill_item Ki e) σ₁ e₂ σ₂ e₃ →
  ∃ e', prim_step e σ₁ e' σ₂ e₃ ∧ e₂ = fill_item Ki e'.
Proof.
  intros ?? Hstep.
  inversion Hstep. destruct K as [|Kj K].
  - simpl in H3, H4; simplify_eq.
    destruct Ki; inversion H5; try naive_solver.
  - simpl in H3, H4; simplify_eq.
    assert (e = fill K e1' ∧ Ki = Kj) as [-> ->].
    { assert (∀ v, (e1' = Val v → False)).
      { intros v ->; by specialize (val_head_stuck _ _ _ _ _ H5). }
      destruct Ki, Kj; try naive_solver;
      try (simpl in H3; simplify_eq;
           by destruct K as [|Ki K]; try destruct Ki, K; try naive_solver).
    }
  exists (fill K e2'). split; [|done].
  by apply (Ectx_prim_step _ _ _ _ _ K e1' e2').
Qed.

Lemma Ectx_prim_step_inv K `{NeutralEctx K} e e₂ e₃ σ₁ σ₂ :
    to_val e = None →
  to_error e = None →
  prim_step (fill K e) σ₁ e₂ σ₂ e₃ →
  ∃ e', prim_step e σ₁ e' σ₂ e₃ ∧ e₂ = fill K e'.
Proof.
  intros ??. revert e₂. induction K as [|Ki K]; intro e₂.
  - simpl. intros Hstep. exists e₂. by split.
  - simpl. intros Hstep.
    have HK:  NeutralEctx  K.  { by apply (neutral_ectx_cons_inv Ki). }
    have HKi: NeutralEctxi Ki. { by apply (neutral_ectx_cons_inv' Ki K). }
    destruct (Ectxi_prim_step_inv Ki _ _ _ _ _ (fill_not_val K _ H1)
                                               (fill_not_error K _ H2) Hstep) as [e' [He' ->]].
    destruct (IHK HK _ He') as [e'' [He'' ->]].
    by exists e''.
Qed.

Lemma pure_prim_step_neutral_ectxi Ki `{NeutralEctxi Ki} e e' :
  pure_prim_step e e' → pure_prim_step (fill_item Ki e) (fill_item Ki e').
Proof.
  constructor.
  - intros ?. by apply reducible_fill_item, H1.
  - intros ???? Hstep.
    have not_val : to_val e = None.
    { by apply (reducible_not_val _ σ1), H1. }
    have not_error : to_error e = None.
    { by apply (reducible_not_error _ σ1), H1. }
    destruct (Ectxi_prim_step_inv Ki e _ _ _ _ not_val not_error Hstep) as [e'' [He'' ->]].
    by destruct (pure_prim_step_det _ _ H1 _ _ _ _ He'') as [-> [-> ->]].
Qed.

Lemma pure_prim_step_neutral_ectx K `{NeutralEctx K} e e' :
  pure_prim_step e e' → pure_prim_step (fill K e) (fill K e').
Proof.
  induction K as [|Ki K]; [done|].
  intros ?. simpl. apply pure_prim_step_neutral_ectxi.
  - by apply (neutral_ectx_cons_inv' _ _ H0).
  - apply IHK; eauto.
    by apply (neutral_ectx_cons_inv  _ _ H0).
Qed.

Lemma reducible_fill_item_inv Ki `{NeutralEctxi Ki} e σ :
    to_val e = None →
  to_error e = None →
  reducible (fill_item Ki e) σ →
  reducible e σ.
Proof.
  intros ??. unfold reducible; simpl; unfold prim_step'; simpl.
  intros [_ [e₂ [σ' [efs Hstep]]]].
  destruct (Ectxi_prim_step_inv _ _ _ _ _ _ H1 H2 Hstep) as [e' [Hstep' _]].
  by exists [], e', σ', efs.
Qed.


(** Bind rule. *)

Lemma ewp_raise_steps K `{NeutralEctx K} Φ₁ Φ₂ v :
  EWP         of_error v  {{ Φ₁ }} {{ Φ₂ }} ⊢
  EWP fill K (of_error v) {{ Φ₁ }} {{ Φ₂ }}.
Proof.
  revert H0. rewrite <-(reverse_involutive K).
  iInduction (reverse K) as [|Ki K'] "IH" forall (Φ₁ Φ₂).
  - by iIntros (_) "H".
  - rewrite reverse_cons fill_app. iIntros (H0) "Hewp".
    assert (NeutralEctx (reverse K') ∧ NeutralEctxi Ki) as [H1 H2].
    { specialize neutral_ectxi_list. rewrite Forall_app.
      intros [H1 H2]. split.
      - constructor. by apply H1.
      - by inversion H2.
    }
    iApply ewp_pure_step.
    apply pure_prim_step_neutral_ectx; [done|].
    by apply pure_prim_step_neutral_ectxi_error.
    by iApply "IH".
Qed.

Lemma ewp_bind K `{NeutralEctx K} Φ₁ Φ₂ e e' :
  e' = fill K e  →
  EWP e {{ λ v, EWP (fill K (of_val v)) {{ Φ₁ }} {{ Φ₂ }} }} {{ Φ₂ }} ⊢
  EWP e' {{ Φ₁ }} {{ Φ₂ }}.
Proof.
  intros ->. iLöb as "IH" forall (e).
    destruct (to_val   e) as [v|] eqn:?;
  [|destruct (to_error e) as [v|] eqn:?].
  - rewrite <- (of_to_val _ _ Heqo).
    iIntros "H". iApply fupd_ewp. by iApply (ewp_value_inv with "H").
  - rewrite <- (of_to_error _ _ Heqo0).
    iIntros "H". iApply ewp_raise_steps.
    iApply ewp_raise_fupd. by iApply (ewp_raise_inv with "H").
  - rewrite !ewp_unfold /ewp_pre Heqo Heqo0.
    rewrite (fill_not_val _ _ Heqo) (fill_not_error K _ Heqo0).
    iIntros "Hewp" (σ₁) "Hs". iMod ("Hewp" $! σ₁ with "Hs") as "[% Hewp]".
    iSplitR; [iPureIntro; by apply reducible_fill|].
    iModIntro. iNext. iIntros (e₂ σ₂ e₃) "%". rename a into Hstep.
    destruct (Ectx_prim_step_inv K _ _ _ _ _ Heqo Heqo0 Hstep) as [e' [Hstep' ->]].
    iMod ("Hewp" $! e' σ₂ e₃ Hstep') as "($ & Hewp & $)".
    by iApply "IH".
Qed.

Lemma Ectxi_ewp_bind Ki `{NeutralEctxi Ki} Φ₁ Φ₂ e e' :
  e' = fill_item Ki e  →
  EWP e {{ λ v, EWP (fill_item Ki (of_val v)) {{ Φ₁ }} {{ Φ₂ }} }} {{ Φ₂ }} ⊢
  EWP e' {{ Φ₁ }} {{ Φ₂ }}.
Proof. intros ->. by iApply (ewp_bind [Ki]). Qed.


(** Try-with rule. *)

Lemma ewp_try_with' Φ₁ Φ₂ e₁ e₂ :
  EWP e₁ {{ λ v, ▷ Φ₁ v }} {{ λ v, ▷ EWP (App e₂ (Val v)) {{ Φ₁ }} {{ Φ₂ }} }} ⊢
  EWP (TryWith e₁ e₂) {{ Φ₁ }} {{ Φ₂ }}.
Proof.
  iLöb as "IH" forall (e₁).
    destruct (to_val   e₁) as [v|] eqn:?;
  [|destruct (to_error e₁) as [v|] eqn:?].
  - rewrite -(of_to_val _ _ Heqo). iIntros "H".
    iApply fupd_ewp. iMod (ewp_value_inv with "H") as "H". iModIntro.
    iApply ewp_pure_step'. apply pure_prim_step_try_with_val.
    iNext. by iApply ewp_value_fupd.
  - rewrite -(of_to_error _ _ Heqo0). iIntros "H".
    iApply fupd_ewp. iMod (ewp_raise_inv with "H") as "H". iModIntro.
    iApply ewp_pure_step'. apply pure_prim_step_try_with_error. done.
  - rewrite !ewp_unfold /ewp_pre Heqo Heqo0.
    iIntros "He₁" (σ₁) "Hs".
    iMod ("He₁" $! σ₁ with "Hs") as "[% He₁]". iModIntro. iSplitR.
    + iPureIntro. revert H0; unfold reducible. simpl.
      rewrite /prim_step'; simpl.
      destruct 1 as [_ [e₄ [σ₄ [efs Hstep]]]].
      inversion Hstep. simplify_eq.
      exists [], (TryWith (fill K e2') e₂), σ₄, efs.
      by apply (Ectx_prim_step _ _ _ _ _ ((TryWithCtx e₂) :: K) e1' e2').
    + iNext. iIntros (e₄ σ₄ e₃) "%". rename a into Hstep.
      assert (Hstep' : ∃ e₅, prim_step e₁ σ₁ e₅ σ₄ e₃ ∧ e₄ = TryWith e₅ e₂).
      { inversion Hstep. destruct K as [|Ki K].
        - simpl in H3; simplify_eq. inversion H3; naive_solver.
        - destruct Ki; try naive_solver. simpl in H1, H2; simplify_eq.
          exists (fill K e2'). simpl. split;[| done].
          by apply (Ectx_prim_step _ _ _ _ _ K e1' e2').
      }
      destruct Hstep' as [e₅ [Hstep' ->]].
      iMod ("He₁" $! e₅ σ₄ e₃ Hstep') as "[$ [He₁ $]]". iModIntro.
      by iApply ("IH" with "He₁").
Qed.

Lemma ewp_try_with Φ₁ Φ₂ e₁ e₂ :
  EWP e₁ {{ λ v, Φ₁ v }} {{ λ v, EWP (App e₂ (Val v)) {{ Φ₁ }} {{ Φ₂ }} }} ⊢
  EWP (TryWith e₁ e₂) {{ Φ₁ }} {{ Φ₂ }}.
Proof.
  iIntros "H". iApply ewp_try_with'.
  by iApply (ewp_mono with "H"); auto.
Qed.


(** Fork rule. *)

Lemma reducible_fork e σ : reducible (Fork e) σ.
Proof.
  unfold reducible; simpl; rewrite /prim_step'; simpl.
  exists [], (Val $ LitV $ LitUnit), σ, [e].
  apply (Ectx_prim_step _ _ _ _ _ [] (Fork e) (Val $ LitV $ LitUnit)); auto.
  by apply ForkS.
Qed.

Lemma fork_det e e₂ e₃ σ₁ σ₂ : 
  prim_step (Fork e) σ₁ e₂ σ₂ e₃ →
  e₂ = Val $ LitV $ LitUnit ∧ σ₁ = σ₂ ∧ e₃ = Some e.
Proof.
  inversion 1.
  destruct K as [|Ki K]; [|destruct Ki; naive_solver].
  simpl in H1, H2. simplify_eq.
  by inversion H3.
Qed.

Lemma ewp_fork e Φ₁ Φ₂ :
  ▷ EWP      e {{ λ _, True }} {{ λ _, False }} -∗ ▷ Φ₁ (LitV LitUnit) -∗
    EWP Fork e {{ Φ₁ }} {{ Φ₂ }}.
Proof.
  iIntros "Hewp HΦ₁". rewrite !(ewp_unfold (Fork e)) /ewp_pre.
  iIntros (σ) "Hs". iModIntro. iSplitR; [iPureIntro; by apply reducible_fork |].
  iNext. iIntros (e₂ σ₂ e₃ Hstep). iModIntro.
  destruct (fork_det _ _ _ _ _ Hstep) as [-> [-> ->]]. iFrame.
  by iApply ewp_value.
Qed.

End ewp.
