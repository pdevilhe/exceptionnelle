From iris.proofmode      Require Import base tactics classes.
From iris.base_logic.lib Require Import iprop wsat.
From iris.algebra        Require Import frac_auth excl_auth.
From exceptionnelle      Require Import lang exn_weakestpre.

(** Weakest Precondition.

  In the preliminary discussion of theory exn_weakest_precondition.v, we have
  seen that the default definition of WP is not well suited for exn_lang.
  However, it is possible to derive a new notion of WP that meets our purposes
  while keeping an unique postcondition. That is exactly what is done here.
  The key intuition is to see the permission to raise an exception as an
  ownable resource. This is realised by the proposition [exn_frag] which behaves
  as follows:

    [▷ Ψ v -∗ exn_frag γ Ψ -∗ WP raise v ; γ {{ λ _, False }}].

  Let's ignore γ for now. The rule indeed agrees with the idea that in order to
  raise an error we need to posses a permission. The postcondition intuitively
  means that after [raise v] comes dead code, therefore any postcondition
  would be sound including the strongest of all: [λ _, False].

  The next rule to look at is the one for the [try with] construct:

    [(exn_frag γ Ψ  -∗ WP     e₁             ; γ {{ λ v, Φ v ∗ exn_frag γ Ψ }})
     -∗
     (∀ w, Ψ w -∗
      exn_frag γ Ψ' -∗ WP             e₂ w   ; γ {{ λ v, Φ v ∗ exn_frag γ Ψ' }})
     -∗
     (exn_frag γ Ψ' -∗ WP try e₁ with e₂ end ; γ {{ λ v, Φ v ∗ exn_frag γ Ψ' }})].

  Its importance is twofold: first, it adds the ability to handle the exception
  raised by the program [e₁] and second, it allows one to shift the permission
  from a predicate [Ψ'] to a new (user-defined) one [Ψ]. Althought, we won't
  cover the details in this informal discussion, we add a side note for the
  experts.


  Side note:

  The ability to shift the permission in the [try-with rule] is one of the
  motivations for defining permissions as an exclusive authoritative camera.
  Ownership of the authoritative element [exn_auth γ Ψ] is given to the user in
  the beginning of the execution of the program as it can be seen in the
  definition of WP:

    [WP e ; γ {{ Φ }} ≜
       ∀ Ψ,
         exn_auth γ Ψ -∗
           EWP e {{ λ v, Φ v ∗ exn_auth γ Ψ }}
                 {{ λ v, Ψ v ∗ exn_auth γ Ψ ∗ exn_frag γ Ψ }}].

  Then, if the user also assumes the ownership of the fragment element
  [exn_frag γ Ψ'], then he can deduce that [Ψ ≡ Ψ'], which is needed to
  establish the second postcondition.


  Mentions:

  The definition of WP shown above was introduced by François Pottier.

*)


(** Picking resources. *)

Class exnG (Σ : gFunctors) := ExnG {
  exn_inG :> inG Σ (excl_authR (laterO (val -d> (iPrePropO Σ))));
}.

Definition exnΣ : gFunctors :=
  GFunctor (authRF (optionURF (exclRF (laterOF (discrete_funOF (λ (_ : val), idOF)))))).

Instance subG_exnΣ {Σ} : subG exnΣ Σ → exnG Σ.
Proof. solve_inG. Qed.


Section wp.

Context `{StateInterp Σ} `{!exnG Σ}.


(** Weakest precondition. *)

Definition exn_auth γ (P : val → iProp Σ) :=
  own γ (●E (Next (iProp_unfold ∘ P) : ofe_car (laterO (val -d> iPrePropO Σ)))%I).

Definition exn_frag γ (P : val → iProp Σ) :=
  own γ (◯E (Next (iProp_unfold ∘ P) : ofe_car (laterO (val -d> iPrePropO Σ)))%I).

Definition wp_def (γ : gname) : expr → (val → iProp Σ) → iProp Σ :=
  λ e Φ₁,
    (∀ Φ₂,
      exn_auth γ Φ₂ -∗ EWP e {{ λ v, Φ₁ v ∗ exn_auth γ Φ₂ }}
                             {{ λ v, Φ₂ v ∗ exn_auth γ Φ₂ ∗ exn_frag γ Φ₂ }})%I.

Notation "'WP' e ; γ {{ Φ } } " := (wp_def γ e%E Φ)
  (at level 200, e, γ, Φ at level 200) : bi_scope.


(** Exn ghost theory. *)

Lemma exn_agree P Q γ' :
  (exn_auth γ' P ∗ exn_frag γ' Q → ▷ ((P : ofe_car (val -d> iPropO Σ)) ≡ Q))%I.
Proof.
  iIntros "[H H']".
  iDestruct (own_valid_2      with "H H'") as "H".
  iDestruct (excl_auth_agreeI with "H")    as "H".
  iNext.
  iApply bi.discrete_fun_equivI. iIntros (v).
  iDestruct (bi.discrete_fun_equivI with "H") as "H".
  iSpecialize ("H" $! v).
  by iApply iProp_unfold_equivI.
Qed.

Lemma exn_agree' P Q γ' :
  (exn_auth γ' P ∗ exn_frag γ' Q →
  ▷ ((iProp_unfold ∘ P : ofe_car (val -d> iPrePropO Σ)) ≡ iProp_unfold ∘ Q))%I.
Proof.
  iIntros "[H H']".
  iDestruct (own_valid_2      with "H H'") as "H".
  iDestruct (excl_auth_agreeI with "H")    as "H".
  by iNext.
Qed.

Lemma exn_agree_ext P Q γ' :
  (exn_auth γ' P ∗ exn_frag γ' Q → ▷ (∀ v, P v ≡ Q v))%I.
Proof.
  iIntros "[H H']" (v).
  iDestruct (own_valid_2      with "H H'") as "H".
  iDestruct (excl_auth_agreeI with "H")    as "H".
  iNext.
  iDestruct (bi.discrete_fun_equivI with "H") as "H".
  iSpecialize ("H" $! v).
  by iApply iProp_unfold_equivI.
Qed.

Lemma rewrite_exn_auth_aux γ P Q :
  (((P : ofe_car (val -d> iPrePropO Σ)) ≡ Q) -∗
  (own γ (●E (Next P)) ≡ own γ (●E (Next Q)) : iProp Σ))%I.
Proof. iIntros "#Heq". by iRewrite "Heq". Qed.

Lemma rewrite_exn_auth γ P Q :
  (((iProp_unfold ∘ P : ofe_car (val -d> iPrePropO Σ)) ≡ iProp_unfold ∘ Q) -∗
  (exn_auth γ P ≡ exn_auth γ Q : iProp Σ))%I.
Proof. iIntros "#Heq". rewrite /exn_auth.  by iApply rewrite_exn_auth_aux. Qed.

Lemma rewrite_exn_auth' γ P Q :
  (((iProp_unfold ∘ P : ofe_car (val -d> iPrePropO Σ)) ≡ iProp_unfold ∘ Q) -∗
  exn_auth γ P -∗ exn_auth γ Q)%I.
Proof.
  iIntros "#Heq". iPoseProof (rewrite_exn_auth γ with "Heq") as "#Heq'".
  iRewrite "Heq'". by iIntros "$".
Qed.

Lemma rewrite_exn_frag_aux γ P Q :
  (((P : ofe_car (val -d> iPrePropO Σ)) ≡ Q) -∗
  (own γ (◯E (Next P)) ≡ own γ (◯E (Next Q)) : iProp Σ))%I.
Proof. iIntros "#Heq". by iRewrite "Heq". Qed.

Lemma rewrite_exn_frag γ P Q :
  (((iProp_unfold ∘ P : ofe_car (val -d> iPrePropO Σ)) ≡ iProp_unfold ∘ Q) -∗
  (exn_frag γ P ≡ exn_frag γ Q : iProp Σ))%I.
Proof. iIntros "#Heq". rewrite /exn_frag.  by iApply rewrite_exn_frag_aux. Qed.

Lemma rewrite_exn_frag' γ P Q :
  (((iProp_unfold ∘ P : ofe_car (val -d> iPrePropO Σ)) ≡ iProp_unfold ∘ Q) -∗
  exn_frag γ P -∗ exn_frag γ Q)%I.
Proof.
  iIntros "#Heq". iPoseProof (rewrite_exn_frag γ with "Heq") as "#Heq'".
  iRewrite "Heq'". by iIntros "$".
Qed.

Lemma exn_alloc P : (|==> ∃ γ', exn_auth γ' P ∗ exn_frag γ' P)%I.
Proof.
  iMod (own_alloc (
       (●E (Next (iProp_unfold ∘ P) : ofe_car (laterO (val -d> iPrePropO Σ)))%I) ⋅
       (◯E (Next (iProp_unfold ∘ P) : ofe_car (laterO (val -d> iPrePropO Σ)))%I)))
       as (γ') "[??]";
  eauto with iFrame.
  apply excl_auth_valid.
Qed.

Lemma exn_update R P Q γ' :
  exn_auth γ' P -∗
  exn_frag γ' Q -∗
  |==> exn_auth γ' R ∗ exn_frag γ' R.
Proof.
  iIntros "H H'". iMod (own_update_2 with "H H'") as "[$ $]"; last done.
  by apply excl_auth_update.
Qed.


(** Weakest precondition rules. *)

Lemma wp_value Φ v γ : Φ v ⊢ WP of_val v ; γ {{ Φ }}.
Proof. rewrite /wp_def. iIntros "H" (Ψ) "Hauth". rewrite ewp_unfold /ewp_pre. by iFrame. Qed.

Lemma wp_value_inv Φ v γ :
  WP of_val v ; γ {{ Φ }} ⊢ (∀ Ψ, exn_auth γ Ψ ==∗ Φ v ∗ exn_auth γ Ψ).
Proof.
  rewrite /wp_def. iIntros "H" (Ψ) "Hauth".
  iApply (ewp_value_inv (λ v, Φ v ∗ exn_auth γ Ψ)%I). by iApply "H".
Qed.

Lemma wp_raise Φ Ψ v γ :
  ▷ Ψ v -∗ exn_frag γ Ψ -∗ WP (Raise $ Val v) ; γ {{ Φ }}.
Proof.
  rewrite /wp_def. iIntros "H Hfrag" (Ψ') "Hauth".
  iApply ewp_pure_step'. apply pure_prim_step_raise.
  iDestruct (exn_agree' Ψ' Ψ with "[$]") as "#Heq".
  iApply ewp_raise. iNext.
  iRewrite (rewrite_exn_frag γ with "Heq").
  iDestruct (bi.discrete_fun_equivI with "Heq") as "Heq'".
  iSpecialize ("Heq'" $! v).
  iRewrite (iProp_unfold_equivI with "Heq'").
  by iFrame.
Qed.

Lemma wp_raise_inv Φ v γ :
  WP of_error v ; γ {{ Φ }} ⊢ (∀ Ψ, exn_auth γ Ψ ==∗ Ψ v ∗ exn_auth γ Ψ ∗ exn_frag γ Ψ).
Proof.
  rewrite /wp_def. iIntros "H" (Ψ) "Hauth".
  iSpecialize ("H" with "Hauth"). by iApply (ewp_raise_inv with "H").
Qed.

Lemma wp_strong_mono Φ Ψ e γ :
  WP e ; γ {{ Φ }} -∗ (∀ v, Φ v ==∗ Ψ v) -∗ WP e ; γ {{ Ψ }}.
Proof.
  rewrite /wp_def. iIntros "H HΦ" (Φ₂) "Hauth".
  iSpecialize ("H" with "Hauth"). iApply (ewp_mono' with "[H] [HΦ]"); auto.
  iIntros (v) "[H Hauth]". iMod ("HΦ" with "H"). by iFrame.
Qed.

Lemma wp_mono Φ Ψ e γ :
  (∀ v, Φ v -∗ Ψ v) →
  WP e ; γ {{ Φ }} ⊢ WP e ; γ {{ Ψ }}.
Proof.
  iIntros (HΦ) "H"; iApply (wp_strong_mono with "H"); auto.
  iIntros (v) "?"; by iApply HΦ.
Qed.

Lemma wp_frame_r Φ R e γ :
  WP e ; γ {{ Φ }} ∗ R ⊢ WP e ; γ {{ λ v, Φ v ∗ R }}.
Proof. iIntros "[H HR]". iApply (wp_strong_mono with "[H] [HR]"); auto with iFrame. Qed.

Lemma wp_frame_l Φ R e γ :
  R ∗ (WP e ; γ {{ Φ }}) ⊢ WP e ; γ {{ λ v, R ∗ Φ v }}.
Proof. iIntros "[HR H]". iApply (wp_strong_mono with "[H] [HR]"); auto with iFrame. Qed.

Lemma fupd_wp e Φ γ : (|==> WP e ; γ {{ Φ }}) ⊢ WP e ; γ {{ Φ }}.
Proof.
  rewrite /wp_def. iIntros "H" (Φ₂) "Hauth".
  iApply fupd_ewp. by iApply "H".
Qed.

Lemma wp_fupd e Φ γ :
  WP e ; γ {{ λ v, |==> Φ v }} ⊢ WP e ; γ {{ Φ }}.
Proof.
  rewrite /wp_def. iIntros "H" (Φ₂) "Hauth".
  iSpecialize ("H" with "Hauth").
  iApply (ewp_strong_mono True%I with "H"); auto.
  iIntros (v) "[H Hauth] _". by iFrame.
Qed.

Lemma wp_value_fupd Φ v γ : (|==> Φ v) ⊢ WP of_val v ; γ {{ Φ }}.
Proof. intros. by rewrite -wp_fupd -wp_value. Qed.

Lemma wp_pure_step' e e' Φ γ :
  pure_prim_step e e' → 
    (▷ WP e' ; γ {{ Φ }}) ⊢ WP e ; γ {{ Φ }}.
Proof.
  rewrite /wp_def. iIntros (Hstep) "H". iIntros (Ψ) "Hauth".
  iApply ewp_pure_step'. apply Hstep. by iApply "H".
Qed.

Lemma wp_pure_step e e' Φ γ :
  pure_prim_step e e' → 
      WP e' ; γ {{ Φ }} ⊢ WP e ; γ {{ Φ }}.
Proof. iIntros "% Hwp". iApply (wp_pure_step' with "[Hwp]"); by auto. Qed.

Lemma wp_bind K `{NeutralEctx K} Φ e e' γ :
  e' = fill K e →
  WP e ; γ {{ λ v, WP fill K (Val v) ; γ {{ Φ }} }} ⊢ WP e' ; γ {{ Φ }}.
Proof.
  intros ->. rewrite /wp_def. iIntros "H" (Φ₂) "Hauth".
  iSpecialize ("H" with "Hauth").
  iApply ewp_bind; auto.
  iApply (ewp_mono with "H"); auto.
  iIntros (v) "[H Hauth]". by iApply "H".
Qed.

Lemma Ectxi_wp_bind Ki `{NeutralEctxi Ki} Φ e e' γ :
  e' = fill_item Ki e  →
  WP e ; γ {{ λ v, WP (fill_item Ki (of_val v)) ; γ {{ Φ }} }} ⊢ WP e' ; γ {{ Φ }}.
Proof. intros ->. by iApply (wp_bind [Ki]). Qed.

Lemma wp_try_with Φ Q Q' e₁ e₂ γ :
  
  ((exn_frag γ Q -∗ WP e₁ ; γ {{ λ v, Φ v ∗ exn_frag γ Q }})

  -∗

  (∀ v, exn_frag γ Q' -∗ Q v -∗ WP (App e₂ (Val v)) ; γ {{ λ v, Φ v ∗ exn_frag γ Q' }})

  -∗

  (exn_frag γ Q' -∗ WP (TryWith e₁ e₂) ; γ {{ λ v, Φ v ∗ exn_frag γ Q' }}))%I.

Proof.
  rewrite /wp_def.
  iIntros "He₁ He₂ Hfrag" (Q'') "Hauth".
  iDestruct (exn_agree' Q'' Q' with "[$]") as "#Heq".
  iApply ewp_try_with'.
  iApply (ewp_strong_mono True%I with "[Hauth Hfrag He₁] [] [] [He₂]").
  - iApply fupd_ewp.
    iMod (exn_update Q Q'' Q' with "Hauth Hfrag") as "[Hauth Hfrag]".
    iModIntro.
    iSpecialize ("He₁" with "Hfrag Hauth").
    iApply "He₁".
  - auto.
  - iIntros (v) "[[$ Hfrag] Hauth] _".
    iMod (exn_update Q' with "Hauth Hfrag") as "[Hauth Hfrag]".
    iModIntro. iNext. iFrame.
    by iRewrite (rewrite_exn_auth γ with "Heq").
  - iIntros (v) "(H & Hauth & Hfrag) _".
    iModIntro. iNext. iApply fupd_ewp.
    iMod (exn_update Q' Q Q with "Hauth Hfrag") as "[Hauth Hfrag]".
    iModIntro.
    iSpecialize ("He₂" with "Hfrag H Hauth").
    iApply (ewp_strong_mono ((iProp_unfold ∘ Q'' : ofe_car (val -d> iPrePropO Σ)) ≡ 
                              iProp_unfold ∘ Q')%I with "He₂"); auto.
    + iIntros (v') "[[$ Hfrag] Hauth] _". iModIntro. iFrame.
      by iRewrite (rewrite_exn_auth γ with "Heq").
    + iIntros (v') "(HQ' & Hfrag & Hauth) _". iModIntro.
      iRewrite (rewrite_exn_auth γ with "Heq").
      iRewrite (rewrite_exn_frag γ with "Heq").
      iDestruct (bi.discrete_fun_equivI with "Heq") as "Heq'".
      iSpecialize ("Heq'" $! v').
      iRewrite (iProp_unfold_equivI with "Heq'").
      by iFrame.
Qed.

Lemma wp_fork e Φ γ :
  ▷ (∀ γ', exn_frag γ' (λ _, False) -∗ WP e ; γ' {{ λ _, True }}) -∗ ▷ Φ (LitV LitUnit) -∗
  WP Fork e ; γ {{ Φ }}.
Proof.
  iIntros "Hwp HΦ" (Φ₂) "Hauth".
  iApply (ewp_fork with "[Hwp]").
  - iNext. iApply fupd_ewp.
    iMod (exn_alloc (λ _, False%I)) as (γ') "[Hauth Hfrag]".
    iModIntro. iSpecialize ("Hwp" with "Hfrag Hauth").
    iApply (ewp_mono with "Hwp"); auto. by iIntros (v) "(H & _ & _)".
  - iNext. by iFrame.
Qed.

End wp.
