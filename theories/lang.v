From stdpp              Require Export binders strings.
From stdpp              Require Import gmap.
From iris.algebra       Require Export ofe.
From iris.program_logic Require Export language.
From iris.heap_lang     Require Export locations.

Set Default Proof Using "Type".

(** exn_lang. An exceptional language.

  Here we define exn_lang, a simple programmeng language with support for
  exceptions. Most of the material was ported from the homonyn file found in the
  heap_lang directory of the Iris project.

  For those familiar with heap_lang, we list some differences between these two:

  - exn_lang does not support arrays.

  - exn_lang does not support the compare-and-swap operation.

  - exn_lang does not support the fetch-and-add operation.

  - exn_lang supports the fork operation.

  - exn_lang does not support prophecies.

  - exn_lang is neither an ectxi_language nor an ectx_language (the proof that
    exn_lang is a language in the iris sense is established directly).


  Contents:

  - Syntax.

    Expressions and values are inhabitants of the inductively defined types
    [expr] and [val] respectively. The definition of shallow evaluation contexts
    corresponds to the type [ectxi]. The type of deep evaluation contexts [ectx]
    is an abbreviation for [list ectxi].

  - Semantics.

    Small-step operational semantincs is defined: a pair of a program and a
    machine state [(e₁, σ₁)] reduces to a seemingly typed pair [(e₂, σ₂)],
    possibly spawning a program [e₃ : option expr] on a new thread, if the
    proposition [prim_step e₁ σ₁ e₂ σ₂ e₃] holds.
    The relation [prim_step] is defined on top of the head reduction relation
    [head_step] as its closure under evaluation contexts:

      [head_step e₁ σ₁ e₂ σ₂ e₃ ⊢ prim_step (fill K e₁) σ₁ (fill K e₂) σ₂ e₃].
    
  - Neutral contexts.

    A neutral shallow context is one that does not block a [RaiseV v] operation
    from ballooning:

      [head_step (fill_item Ki (RaiseV v)) σ (RaiseV v) σ None].

    A neutral deep context is a list of neutral shallow contexts. This concept
    is important for the proof of the bind rule (see theory exn_weakespre.v).

*)

Delimit Scope expr_scope with E.
Delimit Scope val_scope with V.

Module exn_lang.
Open Scope Z_scope.

Inductive base_lit : Set :=
  | LitInt (n : Z) | LitBool (b : bool)
  | LitUnit | LitLoc (l : loc).
Inductive un_op : Set :=
  | NegOp | MinusUnOp.
Inductive bin_op : Set :=
  | PlusOp | MinusOp | MultOp | QuotOp | RemOp (* Arithmetic *)
  | AndOp | OrOp | XorOp (* Bitwise *)
  | ShiftLOp | ShiftROp (* Shifts *)
  | LeOp | LtOp | EqOp (* Relations *)
  | OffsetOp. (* Pointer offset *)

Inductive expr :=
  (* Values *)
  | Val (v : val)
  (* Errors *)
  | Raise  (e : expr)
  | RaiseV (v : val)
  | TryWith (e1 e2 : expr)
  (* Base lambda calculus *)
  | Var (x : string)
  | Rec (f x : binder) (e : expr)
  | App (e1 e2 : expr)
  (* Base types and their operations *)
  | UnOp (op : un_op) (e : expr)
  | BinOp (op : bin_op) (e1 e2 : expr)
  | If (e0 e1 e2 : expr)
  (* Products *)
  | Pair (e1 e2 : expr)
  | Fst (e : expr)
  | Snd (e : expr)
  (* Sums *)
  | InjL (e : expr)
  | InjR (e : expr)
  | Case (e0 e1 e2 : expr)
  (* Concurrency *)
  | Fork (e : expr)
  (* Heap *)
  | Alloc (e : expr) (* initial value *)
  | Load (e : expr)
  | Store (e1 e2 : expr)
with val :=
  | LitV (l : base_lit)
  | RecV (f x : binder) (e : expr)
  | PairV (v1 v2 : val)
  | InjLV (v : val)
  | InjRV (v : val).

Bind Scope expr_scope with expr.
Bind Scope val_scope with val.

Notation of_val := Val (only parsing).

Definition to_val (e : expr) : option val :=
  match e with
  | Val v => Some v
  | _     => None
  end.

Notation of_error := (fun v => RaiseV v) (only parsing).

Definition to_error (e : expr) : option val :=
  match e with
  | RaiseV v => Some v
  | _        => None
  end.

(** The state: heaps of vals. *)
Record state : Type := { heap: gmap loc val }.

(** Equality and other typeclass stuff *)
Lemma to_of_val v : to_val (of_val v) = Some v.
Proof. by destruct v. Qed.

Lemma to_of_error v : to_error (of_error v) = Some v.
Proof. by destruct v. Qed.

Lemma of_to_val e v : to_val e = Some v → of_val v = e.
Proof. destruct e=>//=. by intros [= <-]. Qed.

Lemma of_to_error e v : to_error e = Some v → of_error v = e.
Proof. destruct e=>//=. by intros [= ->]. Qed.

Instance of_val_inj : Inj (=) (=) of_val.
Proof. by intros ?? [=]. Qed.

Instance of_val_error : Inj (=) (=) of_error.
Proof. by intros ?? [=]. Qed.

Instance base_lit_eq_dec : EqDecision base_lit.
Proof. solve_decision. Defined.
Instance un_op_eq_dec : EqDecision un_op.
Proof. solve_decision. Defined.
Instance bin_op_eq_dec : EqDecision bin_op.
Proof. solve_decision. Defined.
Instance expr_eq_dec : EqDecision expr.
Proof.
  refine (
   fix go (e1 e2 : expr) {struct e1} : Decision (e1 = e2) :=
     match e1, e2 with
     | Val v, Val v' => cast_if (decide (v = v'))
     | Var x, Var x' => cast_if (decide (x = x'))
     | RaiseV v, RaiseV v'=> cast_if (decide (v = v'))
     | Raise e, Raise e'=> cast_if (decide (e = e'))
     | TryWith e1 e2, TryWith e1' e2' =>
        cast_if_and (decide (e1 = e1')) (decide (e2 = e2'))
     | Rec f x e, Rec f' x' e' =>
        cast_if_and3 (decide (f = f')) (decide (x = x')) (decide (e = e'))
     | App e1 e2, App e1' e2' => cast_if_and (decide (e1 = e1')) (decide (e2 = e2'))
     | UnOp o e, UnOp o' e' => cast_if_and (decide (o = o')) (decide (e = e'))
     | BinOp o e1 e2, BinOp o' e1' e2' =>
        cast_if_and3 (decide (o = o')) (decide (e1 = e1')) (decide (e2 = e2'))
     | If e0 e1 e2, If e0' e1' e2' =>
        cast_if_and3 (decide (e0 = e0')) (decide (e1 = e1')) (decide (e2 = e2'))
     | Pair e1 e2, Pair e1' e2' =>
        cast_if_and (decide (e1 = e1')) (decide (e2 = e2'))
     | Fst e, Fst e' => cast_if (decide (e = e'))
     | Snd e, Snd e' => cast_if (decide (e = e'))
     | InjL e, InjL e' => cast_if (decide (e = e'))
     | InjR e, InjR e' => cast_if (decide (e = e'))
     | Case e0 e1 e2, Case e0' e1' e2' =>
        cast_if_and3 (decide (e0 = e0')) (decide (e1 = e1')) (decide (e2 = e2'))
     | Fork e, Fork e' => cast_if (decide (e = e'))
     | Alloc e, Alloc e' => cast_if (decide (e = e'))
     | Load e, Load e' => cast_if (decide (e = e'))
     | Store e1 e2, Store e1' e2' =>
        cast_if_and (decide (e1 = e1')) (decide (e2 = e2'))
     | _, _ => right _
     end
   with gov (v1 v2 : val) {struct v1} : Decision (v1 = v2) :=
     match v1, v2 with
     | LitV l, LitV l' => cast_if (decide (l = l'))
     | RecV f x e, RecV f' x' e' =>
        cast_if_and3 (decide (f = f')) (decide (x = x')) (decide (e = e'))
     | PairV e1 e2, PairV e1' e2' =>
        cast_if_and (decide (e1 = e1')) (decide (e2 = e2'))
     | InjLV e, InjLV e' => cast_if (decide (e = e'))
     | InjRV e, InjRV e' => cast_if (decide (e = e'))
     | _, _ => right _
     end
   for go); try (clear go gov; abstract intuition congruence).
Defined.
Instance val_eq_dec : EqDecision val.
Proof. solve_decision. Defined.

Instance base_lit_countable : Countable base_lit.
Proof.
 refine (inj_countable' (λ l, match l with
  | LitInt n => inl (inl n)
  | LitBool b => inl (inr b)
  | LitUnit => inr (inl ())
  | LitLoc l => inr (inr l)
  end) (λ l, match l with
  | inl (inl n) => LitInt n
  | inl (inr b) => LitBool b
  | inr (inl ()) => LitUnit
  | inr (inr l) => LitLoc l
  end) _); by intros [].
Qed.
Instance un_op_finite : Countable un_op.
Proof.
 refine (inj_countable' (λ op, match op with NegOp => 0 | MinusUnOp => 1 end)
  (λ n, match n with 0 => NegOp | _ => MinusUnOp end) _); by intros [].
Qed.
Instance bin_op_countable : Countable bin_op.
Proof.
 refine (inj_countable' (λ op, match op with
  | PlusOp => 0 | MinusOp => 1 | MultOp => 2 | QuotOp => 3 | RemOp => 4
  | AndOp => 5 | OrOp => 6 | XorOp => 7 | ShiftLOp => 8 | ShiftROp => 9
  | LeOp => 10 | LtOp => 11 | EqOp => 12 | OffsetOp => 13
  end) (λ n, match n with
  | 0 => PlusOp | 1 => MinusOp | 2 => MultOp | 3 => QuotOp | 4 => RemOp
  | 5 => AndOp | 6 => OrOp | 7 => XorOp | 8 => ShiftLOp | 9 => ShiftROp
  | 10 => LeOp | 11 => LtOp | 12 => EqOp | _ => OffsetOp
  end) _); by intros [].
Qed.
Instance expr_countable : Countable expr.
Proof.
 set (enc :=
   fix go e :=
     match e with
     | Val v => GenNode 0 [gov v]
     | Var x => GenLeaf (inl (inl x))
     | Rec f x e => GenNode 1 [GenLeaf (inl (inr f)); GenLeaf (inl (inr x)); go e]
     | App e1 e2 => GenNode 2 [go e1; go e2]
     | UnOp op e => GenNode 3 [GenLeaf (inr (inr (inl op))); go e]
     | BinOp op e1 e2 => GenNode 4 [GenLeaf (inr (inr (inr op))); go e1; go e2]
     | If e0 e1 e2 => GenNode 5 [go e0; go e1; go e2]
     | Pair e1 e2 => GenNode 6 [go e1; go e2]
     | Fst e => GenNode 7 [go e]
     | Snd e => GenNode 8 [go e]
     | InjL e => GenNode 9 [go e]
     | InjR e => GenNode 10 [go e]
     | Case e0 e1 e2 => GenNode 11 [go e0; go e1; go e2]
     | Alloc e => GenNode 12 [go e]
     | Load e => GenNode 13 [go e]
     | Store e1 e2 => GenNode 14 [go e1; go e2]
     | Raise e => GenNode 15 [go e]
     | RaiseV v => GenNode 16 [gov v]
     | TryWith e1 e2 => GenNode 17 [go e1; go e2]
     | Fork e => GenNode 18 [go e]
     end
   with gov v :=
     match v with
     | LitV l => GenLeaf (inr (inl l))
     | RecV f x e =>
        GenNode 0 [GenLeaf (inl (inr f)); GenLeaf (inl (inr x)); go e]
     | PairV v1 v2 => GenNode 1 [gov v1; gov v2]
     | InjLV v => GenNode 2 [gov v]
     | InjRV v => GenNode 3 [gov v]
     end
   for go).
 set (dec :=
   fix go e :=
     match e with
     | GenNode 0 [v] => Val (gov v)
     | GenLeaf (inl (inl x)) => Var x
     | GenNode 1 [GenLeaf (inl (inr f)); GenLeaf (inl (inr x)); e] => Rec f x (go e)
     | GenNode 2 [e1; e2] => App (go e1) (go e2)
     | GenNode 3 [GenLeaf (inr (inr (inl op))); e] => UnOp op (go e)
     | GenNode 4 [GenLeaf (inr (inr (inr op))); e1; e2] => BinOp op (go e1) (go e2)
     | GenNode 5 [e0; e1; e2] => If (go e0) (go e1) (go e2)
     | GenNode 6 [e1; e2] => Pair (go e1) (go e2)
     | GenNode 7 [e] => Fst (go e)
     | GenNode 8 [e] => Snd (go e)
     | GenNode 9 [e] => InjL (go e)
     | GenNode 10 [e] => InjR (go e)
     | GenNode 11 [e0; e1; e2] => Case (go e0) (go e1) (go e2)
     | GenNode 12 [e] => Alloc (go e)
     | GenNode 13 [e] => Load (go e)
     | GenNode 14 [e1; e2] => Store (go e1) (go e2)
     | GenNode 15 [e] => Raise (go e)
     | GenNode 16 [v] => RaiseV (gov v)
     | GenNode 17 [e0; e1] => TryWith (go e0) (go e1)
     | GenNode 18 [e] => Fork (go e)
     | _ => Val $ LitV LitUnit (* dummy *)
     end
   with gov v :=
     match v with
     | GenLeaf (inr (inl l)) => LitV l
     | GenNode 0 [GenLeaf (inl (inr f)); GenLeaf (inl (inr x)); e] => RecV f x (go e)
     | GenNode 1 [v1; v2] => PairV (gov v1) (gov v2)
     | GenNode 2 [v] => InjLV (gov v)
     | GenNode 3 [v] => InjRV (gov v)
     | _ => LitV LitUnit (* dummy *)
     end
   for go).
 refine (inj_countable' enc dec _).
 refine (fix go (e : expr) {struct e} := _ with gov (v : val) {struct v} := _ for go).
 - destruct e as [v| | | | | | | | | | | | | | | | | | |]; simpl; f_equal;
     [exact (gov v)|done..].
 - destruct v; by f_equal.
Qed.
Instance val_countable : Countable val.
Proof. refine (inj_countable of_val to_val _); auto using to_of_val. Qed.

Instance state_inhabited : Inhabited state := populate {| heap := inhabitant |}.
Instance val_inhabited : Inhabited val := populate (LitV LitUnit).
Instance expr_inhabited : Inhabited expr := populate (Val inhabitant).

(** Evaluation contexts *)
Inductive ectx_item :=
  | AppLCtx (v2 : val)
  | AppRCtx (e1 : expr)
  | RaiseCtx
  | TryWithCtx (e2 : expr)
  | UnOpCtx (op : un_op)
  | BinOpLCtx (op : bin_op) (v2 : val)
  | BinOpRCtx (op : bin_op) (e1 : expr)
  | IfCtx (e1 e2 : expr)
  | PairLCtx (v2 : val)
  | PairRCtx (e1 : expr)
  | FstCtx
  | SndCtx
  | InjLCtx
  | InjRCtx
  | CaseCtx (e1 e2 : expr)
  | AllocCtx
  | LoadCtx
  | StoreLCtx (v2 : val)
  | StoreRCtx (e1 : expr).

(** Contextual closure *)
Fixpoint fill_item (Ki : ectx_item) (e : expr) : expr :=
  match Ki with
  | AppLCtx v2 => App e (of_val v2)
  | AppRCtx e1 => App e1 e
  | RaiseCtx => Raise e
  | TryWithCtx e2 => TryWith e e2
  | UnOpCtx op => UnOp op e
  | BinOpLCtx op v2 => BinOp op e (Val v2)
  | BinOpRCtx op e1 => BinOp op e1 e
  | IfCtx e1 e2 => If e e1 e2
  | PairLCtx v2 => Pair e (Val v2)
  | PairRCtx e1 => Pair e1 e
  | FstCtx => Fst e
  | SndCtx => Snd e
  | InjLCtx => InjL e
  | InjRCtx => InjR e
  | CaseCtx e1 e2 => Case e e1 e2
  | AllocCtx => Alloc e
  | LoadCtx => Load e
  | StoreLCtx v2 => Store e (Val v2)
  | StoreRCtx e1 => Store e1 e
  end.

(** Contexts *)
Definition ectx := (list ectx_item).

Definition fill (K : ectx) : expr → expr := λ e, foldr fill_item e K.

Lemma fill_cons Ki K e : fill (Ki :: K) e = fill_item Ki (fill K e).
Proof. done. Qed.

Lemma fill_app K K' e : fill (K ++ K') e = fill K (fill K' e).
Proof. by rewrite /fill fold_right_app. Qed.

(** Substitution *)
Fixpoint subst (x : string) (v : val) (e : expr)  : expr :=
  match e with
  | Val _ => e
  | Var y => if decide (x = y) then Val v else Var y
  | Raise e => Raise (subst x v e)
  | RaiseV _ => e
  | TryWith e1 e2 => TryWith (subst x v e1) (subst x v e2)
  | Rec f y e =>
     Rec f y $ if decide (BNamed x ≠ f ∧ BNamed x ≠ y) then subst x v e else e
  | App e1 e2 => App (subst x v e1) (subst x v e2)
  | UnOp op e => UnOp op (subst x v e)
  | BinOp op e1 e2 => BinOp op (subst x v e1) (subst x v e2)
  | If e0 e1 e2 => If (subst x v e0) (subst x v e1) (subst x v e2)
  | Pair e1 e2 => Pair (subst x v e1) (subst x v e2)
  | Fst e => Fst (subst x v e)
  | Snd e => Snd (subst x v e)
  | InjL e => InjL (subst x v e)
  | InjR e => InjR (subst x v e)
  | Fork e => Fork (subst x v e)
  | Case e0 e1 e2 => Case (subst x v e0) (subst x v e1) (subst x v e2)
  | Alloc e => Alloc (subst x v e)
  | Load e => Load (subst x v e)
  | Store e1 e2 => Store (subst x v e1) (subst x v e2)
  end.

Definition subst' (mx : binder) (v : val) : expr → expr :=
  match mx with BNamed x => subst x v | BAnon => id end.

(** The stepping relation *)
Definition un_op_eval (op : un_op) (v : val) : option val :=
  match op, v with
  | NegOp, LitV (LitBool b) => Some $ LitV $ LitBool (negb b)
  | NegOp, LitV (LitInt n) => Some $ LitV $ LitInt (Z.lnot n)
  | MinusUnOp, LitV (LitInt n) => Some $ LitV $ LitInt (- n)
  | _, _ => None
  end.

Definition bin_op_eval_int (op : bin_op) (n1 n2 : Z) : option base_lit :=
  match op with
  | PlusOp => Some $ LitInt (n1 + n2)
  | MinusOp => Some $ LitInt (n1 - n2)
  | MultOp => Some $ LitInt (n1 * n2)
  | QuotOp => Some $ LitInt (n1 `quot` n2)
  | RemOp => Some $ LitInt (n1 `rem` n2)
  | AndOp => Some $ LitInt (Z.land n1 n2)
  | OrOp => Some $ LitInt (Z.lor n1 n2)
  | XorOp => Some $ LitInt (Z.lxor n1 n2)
  | ShiftLOp => Some $ LitInt (n1 ≪ n2)
  | ShiftROp => Some $ LitInt (n1 ≫ n2)
  | LeOp => Some $ LitBool (bool_decide (n1 ≤ n2))
  | LtOp => Some $ LitBool (bool_decide (n1 < n2))
  | EqOp => Some $ LitBool (bool_decide (n1 = n2))
  | OffsetOp => None (* Pointer arithmetic *)
  end.

Definition bin_op_eval_bool (op : bin_op) (b1 b2 : bool) : option base_lit :=
  match op with
  | PlusOp | MinusOp | MultOp | QuotOp | RemOp => None (* Arithmetic *)
  | AndOp => Some (LitBool (b1 && b2))
  | OrOp => Some (LitBool (b1 || b2))
  | XorOp => Some (LitBool (xorb b1 b2))
  | ShiftLOp | ShiftROp => None (* Shifts *)
  | LeOp | LtOp => None (* InEquality *)
  | EqOp => Some (LitBool (bool_decide (b1 = b2)))
  | OffsetOp => None (* Pointer arithmetic *)
  end.

Definition bin_op_eval (op : bin_op) (v1 v2 : val) : option val :=
  if decide (op = EqOp) then
    Some $ LitV $ LitBool $ bool_decide (v1 = v2)
  else
    match v1, v2 with
    | LitV (LitInt n1), LitV (LitInt n2) => LitV <$> bin_op_eval_int op n1 n2
    | LitV (LitBool b1), LitV (LitBool b2) => LitV <$> bin_op_eval_bool op b1 b2
    | LitV (LitLoc l), LitV (LitInt off) => Some $ LitV $ LitLoc (l +ₗ off)
    | _, _ => None
    end.

Definition state_upd_heap (f: gmap loc val → gmap loc val) (σ: state) : state :=
  {| heap := f σ.(heap) |}.
Arguments state_upd_heap _ !_ /.

Inductive head_step : expr → state → expr → state → option expr → Prop :=
  (* Lambda. *)
  | RecS f x e σ :
     head_step (Rec f x e) σ (Val $ RecV f x e) σ None
  (* Pair. *)
  | PairS v1 v2 σ :
     head_step (Pair (Val v1) (Val v2)) σ (Val $ PairV v1 v2) σ None
  (* InjL. *)
  | InjLS v σ :
     head_step (InjL $ Val v) σ (Val $ InjLV v) σ None
  (* InjR. *)
  | InjRS v σ :
     head_step (InjR $ Val v) σ (Val $ InjRV v) σ None
  (* Beta reduction. *)
  | BetaS f x e1 v2 e' σ :
     e' = subst' x v2 (subst' f (RecV f x e1) e1) →
     head_step (App (Val $ RecV f x e1) (Val v2)) σ e' σ None
  (* UnOp. *)
  | UnOpS op v v' σ :
     un_op_eval op v = Some v' →
     head_step (UnOp op (Val v)) σ (Val v') σ None
  (* BinOp. *)
  | BinOpS op v1 v2 v' σ :
     bin_op_eval op v1 v2 = Some v' →
     head_step (BinOp op (Val v1) (Val v2)) σ (Val v') σ None
  (* If. *)
  | IfTrueS e1 e2 σ :
     head_step (If (Val $ LitV $ LitBool true) e1 e2) σ e1 σ None
  | IfFalseS e1 e2 σ :
     head_step (If (Val $ LitV $ LitBool false) e1 e2) σ e2 σ None
  (* Fst. *)
  | FstS v1 v2 σ :
     head_step (Fst (Val $ PairV v1 v2)) σ (Val v1) σ None
  (* Snd. *)
  | SndS v1 v2 σ :
     head_step (Snd (Val $ PairV v1 v2)) σ (Val v2) σ None
  (* Case. *)
  | CaseLS v e1 e2 σ :
     head_step (Case (Val $ InjLV v) e1 e2) σ (App e1 (Val v)) σ None
  | CaseRS v e1 e2 σ :
     head_step (Case (Val $ InjRV v) e1 e2) σ (App e2 (Val v)) σ None
  (* Fork. *)
  | ForkS e σ :
     head_step (Fork e) σ (Val $ LitV $ LitUnit) σ (Some e)
  (* Alloc. *)
  | AllocS v σ l :
     σ.(heap) !! l = None →
     head_step (Alloc (Val v))          σ
               (Val $ LitV $ LitLoc l) (state_upd_heap <[l:=v]> σ) None
  (* Load. *)
  | LoadS l v σ :
     σ.(heap) !! l = Some v →
     head_step (Load (Val $ LitV $ LitLoc l)) σ (of_val v) σ None
  (* Store. *)
  | StoreS l v σ :
     is_Some (σ.(heap) !! l) →
     head_step (Store (Val $ LitV $ LitLoc l) (Val v)) σ
               (Val $ LitV LitUnit) (state_upd_heap <[l:=v]> σ) None
  (* TryWith. *)
  | TryWithRaiseS v e σ :
     head_step (TryWith (RaiseV v) e) σ (App e (Val v)) σ None
  | TryWithValS v e σ :
     head_step (TryWith (Val v) e) σ (Val v) σ None
  (* RaiseCtx. *)
  | RaiseS v σ :
     head_step (Raise $ Val v) σ (RaiseV v) σ None
  (* AppLCtx: [ raise v1 ] v2 --> raise v1. *)
  | AppRaiseLS v1 v2 σ :
     head_step (App (RaiseV v1) (Val v2)) σ (RaiseV v1) σ None
  (* AppRCtx:  e1 [ raise v2 ] --> raise v2. *)
  | AppRaiseRS e1 v2 σ :
     head_step (App e1 (RaiseV v2)) σ (RaiseV v2) σ None
  (* UnOpCtx: op [ raise v ] --> raise v. *)
  | UnOpRaiseS op v σ :
     head_step (UnOp op (RaiseV v)) σ (RaiseV v) σ None
  (* BinOpLCtx: op [ raise v1 ] v2 --> raise v1. *)
  | BinOpRaiseLS op v1 v2 σ :
     head_step (BinOp op (RaiseV v1) (Val v2)) σ (RaiseV v1) σ None
  (* BinOpRCtx: op e1 [ raise v2 ] --> raise v2. *)
  | BinOpRaiseRS op e1 v2 σ :
     head_step (BinOp op e1 (RaiseV v2)) σ (RaiseV v2) σ None
  (* IfCtx: If [ raise v ] e1 e2 --> raise v. *)
  | IfRaiseS v e1 e2 σ :
     head_step (If (RaiseV v) e1 e2) σ (RaiseV v) σ None
  (* PairLCtx: ([ raise v1 ], v2) --> raise v1. *)
  | PairRaiseLS v1 v2 σ :
     head_step (Pair (RaiseV v1) (Val v2)) σ (RaiseV v1) σ None
  (* PairRCtx: (e1, [ raise v2 ]) --> raise v2. *)
  | PairRaiseRS e1 v2 σ :
     head_step (Pair e1 (RaiseV v2)) σ (RaiseV v2) σ None
  (* FstCtx: fst [ raise v ] --> raise v. *)
  | FstRaiseS v σ :
     head_step (Fst (RaiseV v)) σ (RaiseV v) σ None
  (* SndCtx: snd [ raise v ] --> raise v. *)
  | SndRaiseS v σ :
     head_step (Snd (RaiseV v)) σ (RaiseV v) σ None
  (* InjLCtx: InjL [ raise v ] --> raise v. *)
  | InjRaiseLS v σ :
     head_step (InjL $ RaiseV v) σ (RaiseV v) σ None
  (* InjRCtx: InjL [ raise v ] --> raise v. *)
  | InjRaiseRS v σ :
     head_step (InjR $ RaiseV v) σ (RaiseV v) σ None
  (* CaseCtx: match [ raise v ] with InjL => e1 | InjR => e2 end --> raise v. *)
  | CaseRaiseS v e1 e2 σ :
     head_step (Case (RaiseV v) e1 e2) σ (RaiseV v) σ None
  (* AllocCtx: ref [ raise v ] --> raise v. *)
  | AllocRaiseS v σ :
     head_step (Alloc $ RaiseV v) σ (RaiseV v) σ None
  (* LoadCtx: ! [ raise v ] --> raise v. *)
  | LoadRaiseS v σ :
     head_step (Load (RaiseV v)) σ (RaiseV v) σ None
  (* StoreLCtx: [ raise v1 ] := v2 --> raise v1. *)
  | StoreRaiseLS v1 v2 σ :
     head_step (Store (RaiseV v1) (Val v2)) σ (RaiseV v1) σ None
  (* StoreRCtx: e1 := [ raise v2 ] --> raise v2. *)
  | StoreRaiseRS e1 v2 σ :
     head_step (Store e1 (RaiseV v2)) σ (RaiseV v2) σ None
  (* RaiseCtx: raise [ raise v ] --> raise v. *)
  | RaiseRaiseS v σ :
     head_step (Raise $ RaiseV v) σ (RaiseV v) σ None.


Inductive prim_step (e1 : expr) (σ1 : state)
                    (e2 : expr) (σ2 : state) (e3 : option expr) : Prop :=
  | Ectx_prim_step K e1' e2' :
     e1 = fill K e1' →
     e2 = fill K e2' →
     head_step e1' σ1 e2' σ2 e3 →
     prim_step e1  σ1 e2  σ2 e3.

Lemma Ectxi_prim_step Ki e1' e2' e1 σ1 e2 σ2 e3 :
  e1 = fill_item Ki e1' →
  e2 = fill_item Ki e2' →
  head_step e1' σ1 e2' σ2 e3 →
  prim_step e1  σ1 e2  σ2 e3.
Proof. intros -> -> H. by apply (Ectx_prim_step _ _ _ _ _ [Ki] e1' e2'). Qed.

Lemma Ectxi_prim_step' Ki e1' e2' e1 σ1 e2 σ2 e3 :
  e1 = fill_item Ki e1' →
  e2 = fill_item Ki e2' →
  prim_step e1' σ1 e2' σ2 e3 →
  prim_step e1  σ1 e2  σ2 e3.
Proof.
  intros -> ->. inversion 1. simplify_eq.
  by apply (Ectx_prim_step _ _ _ _ _ (Ki :: K) e1'0 e2'0).
Qed.

Lemma Ectx_prim_step' K e1 σ1 e2 σ2 e1' e2' e3 :
  e1 = fill K e1' →
  e2 = fill K e2' →
  prim_step e1' σ1 e2' σ2 e3 →
  prim_step e1  σ1 e2  σ2 e3.
Proof.
  intros -> ->. inversion 1. simplify_eq.
  apply (Ectx_prim_step _ _ _ _ _ (K ++ K0) e1'0 e2'0); eauto; by rewrite fill_app.
Qed.


Definition prim_step' e1 σ1 (obs : list unit) e2 σ2 (efs : list expr) :=
  prim_step e1 σ1 e2 σ2 (head efs).

(** Basic properties about the language *)
Instance fill_item_inj Ki : Inj (=) (=) (fill_item Ki).
Proof. induction Ki; intros ???; simplify_eq/=; auto with f_equal. Qed.

Lemma fill_item_val Ki e : to_val (fill_item Ki e) = None.
Proof. induction Ki; simplify_option_eq; eauto. Qed.

Lemma fill_val K e v : to_val (fill K e) = Some v → K = [].
Proof. destruct K as [|Ki K]; try destruct Ki; by naive_solver. Qed.

Lemma fill_val' K e v : fill K e = Val v → K = [].
Proof. intros ?; apply (fill_val _ e v). by rewrite H. Qed.

Lemma fill_item_error Ki e : to_error (fill_item Ki e) = None.
Proof. induction Ki; simplify_option_eq; eauto. Qed.

Lemma fill_error K e v : to_error (fill K e) = Some v → K = [].
Proof. destruct K as [|Ki K]; try destruct Ki; by naive_solver. Qed.

Lemma fill_error' K e v : fill K e = (RaiseV v) → K = [].
Proof. intros ?; apply (fill_error _ e v). by rewrite H. Qed.

Lemma fill_not_val K e : to_val e = None → to_val (fill K e) = None.
Proof.
  induction K as [|Ki K]; eauto.
  induction Ki; simplify_option_eq; eauto.
Qed.

Lemma fill_not_error K e : to_error e = None → to_error (fill K e) = None.
Proof.
  induction K as [|Ki K]; eauto.
  induction Ki; simplify_option_eq; eauto.
Qed.

Lemma val_head_stuck e1 σ1 e2 σ2 e3 : head_step e1 σ1 e2 σ2 e3 → to_val e1 = None.
Proof. destruct 1; eauto. Qed.

Lemma error_head_stuck e1 σ1 e2 σ2 e3 : head_step e1 σ1 e2 σ2 e3 → to_error e1 = None.
Proof. destruct 1; eauto. Qed.

Lemma fill_item_no_val_inj Ki1 Ki2 e1 e2 :
  to_val e1 = None → to_val e2 = None →
  fill_item Ki1 e1 = fill_item Ki2 e2 → Ki1 = Ki2.
Proof. revert Ki1. induction Ki2, Ki1; naive_solver eauto with f_equal. Qed.

Lemma alloc_fresh v σ :
  let l := fresh_locs (dom (gset loc) σ.(heap)) in
  head_step (Alloc (Val v))         σ
            (Val $ LitV $ LitLoc l) (state_upd_heap <[l:=v]> σ) None.
Proof.
  intros.
  apply AllocS.
  intros. apply (not_elem_of_dom (D := gset loc)).
  specialize (fresh_locs_fresh (dom _ (heap σ)) 0).
  rewrite loc_add_0. naive_solver.
Qed.

Lemma exn_lang_mixin : LanguageMixin of_val to_val prim_step'.
Proof.
  split; apply _ || eauto using to_of_val, of_to_val.
  unfold prim_step'.
  intros e σ _ e' σ' efs.
  induction 1 as [K e1' e2' -> ? ?].
  case K as [|Ki K].
  - revert H0. apply val_head_stuck.
  - apply fill_item_val.
Qed.
End exn_lang.

(** Language *)

Canonical Structure exn_lang : language := Language exn_lang.exn_lang_mixin.

(* Prefer exn_lang names over ectx_language names. *)
Export exn_lang.

Lemma to_val_fill_some K e v : to_val (fill K e) = Some v → K = [] ∧ e = Val v.
Proof.
  intro H. specialize (fill_val _ _ _ H) as ->.
  by split; last (symmetry; apply of_to_val).
Qed.

Lemma prim_step_to_val_is_head_step e σ1 w σ2 e3 :
  prim_step e σ1 (Val w) σ2 e3 → head_step e σ1 (Val w) σ2 e3.
Proof.
  intro H. destruct H as [K e1 e2 H1 H2].
  assert (to_val (fill K e2) = Some w) as H3; first by rewrite -H2.
  apply to_val_fill_some in H3 as [-> ->]. subst e. done.
Qed.

(** If [e1] makes a head step to a value under some state [σ1] then any head
 step from [e1] under any other state [σ1'] must necessarily be to a value. *)
Lemma head_step_to_val e1 σ1 e2 σ2 e3 σ1' e2' σ2' e3' :
  head_step e1 σ1  e2  σ2  e3  →
  head_step e1 σ1' e2' σ2' e3' →
  is_Some (to_val e2) →
  is_Some (to_val e2').
Proof. destruct 1; inversion 1; try naive_solver. Qed.


(* Neutral Evaluation Contexts. *)

Class NeutralEctxi (Ki : ectx_item) :=
  { neutral_ectxi v σ : head_step (fill_item Ki (RaiseV v)) σ (RaiseV v) σ None }.
Class NeutralEctx (K : ectx) :=
  { neutral_ectxi_list : Forall NeutralEctxi K }.

Instance neutral_ectx_nil : NeutralEctx [].
Proof. constructor. by apply Forall_nil. Qed.
Instance neutral_ectx_cons Ki K : NeutralEctxi Ki → NeutralEctx K → NeutralEctx (Ki :: K).
Proof. constructor. apply Forall_cons. split; [|apply H0]; done. Qed.
Lemma neutral_ectx_cons_inv Ki K : NeutralEctx (Ki :: K) → NeutralEctx K.
Proof. constructor. by apply (Forall_inv_tail neutral_ectxi_list). Qed.
Lemma neutral_ectx_cons_inv' Ki K : NeutralEctx (Ki :: K) → NeutralEctxi Ki.
Proof. intros ?. cut (Forall NeutralEctxi (Ki :: K)). by inversion 1. by apply H. Qed.

Instance NeutralAppLCtx v2 : NeutralEctxi (AppLCtx v2).
Proof. constructor => v σ. by apply AppRaiseLS. Qed.
Instance NeutralAppRCtx e1 : NeutralEctxi (AppRCtx e1).
Proof. constructor => v σ. by apply AppRaiseRS. Qed.
Instance NeutralRaiseCtx : NeutralEctxi RaiseCtx.
Proof. constructor => v σ. by apply RaiseRaiseS. Qed.
Instance NeutralUnOpCtx op : NeutralEctxi (UnOpCtx op).
Proof. constructor => v σ. by apply UnOpRaiseS. Qed.
Instance NeutralBinOpLCtx op v2 : NeutralEctxi (BinOpLCtx op v2).
Proof. constructor => v σ. by apply BinOpRaiseLS. Qed.
Instance NeutralBinOpRCtx op e1 : NeutralEctxi (BinOpRCtx op e1).
Proof. constructor => v σ. by apply BinOpRaiseRS. Qed.
Instance NeutralIfCtx e1 e2 : NeutralEctxi (IfCtx e1 e2).
Proof. constructor => v σ. by apply IfRaiseS. Qed.
Instance NeutralPairLCtx v2 : NeutralEctxi (PairLCtx v2).
Proof. constructor => v σ. by apply PairRaiseLS. Qed.
Instance NeutralPairRCtx e1 : NeutralEctxi (PairRCtx e1).
Proof. constructor => v σ. by apply PairRaiseRS. Qed.
Instance NeutralFstCtx : NeutralEctxi FstCtx.
Proof. constructor => v σ. by apply FstRaiseS. Qed.
Instance NeutralSndCtx : NeutralEctxi SndCtx.
Proof. constructor => v σ. by apply SndRaiseS. Qed.
Instance NeutralInjLCtx : NeutralEctxi InjLCtx.
Proof. constructor => v σ. by apply InjRaiseLS. Qed.
Instance NeutralInjRCtx : NeutralEctxi InjRCtx.
Proof. constructor => v σ. by apply InjRaiseRS. Qed.
Instance NeutralCaseCtx e1 e2 : NeutralEctxi (CaseCtx e1 e2).
Proof. constructor => v σ. by apply CaseRaiseS. Qed.
Instance NeutralAllocCtx : NeutralEctxi AllocCtx.
Proof. constructor => v σ. by apply AllocRaiseS. Qed.
Instance NeutralLoadCtx : NeutralEctxi LoadCtx.
Proof. constructor => v σ. by apply LoadRaiseS. Qed.
Instance NeutralStoreLCtx v2 : NeutralEctxi (StoreLCtx v2).
Proof. constructor => v σ. by apply StoreRaiseLS. Qed.
Instance NeutralStoreRCtx e1 : NeutralEctxi (StoreRCtx e1).
Proof. constructor => v σ. by apply StoreRaiseRS. Qed.


(** Reducible *)

Lemma reducible_fill_item_raise Ki v σ :
  reducible (fill_item Ki (Raise (Val v))) σ.
Proof.
  unfold reducible. simpl. exists [], (fill_item Ki (RaiseV v)), σ, [].
  apply (Ectx_prim_step _ _ _ _ _ [Ki] (Raise $ Val v) (RaiseV      v)); eauto.
  by apply RaiseS.
Qed.

Lemma reducible_fill_item_raiseV Ki `{NeutralEctxi Ki} v σ :
  reducible (fill_item Ki (RaiseV v)) σ.
Proof.
  unfold reducible. simpl. exists [], (RaiseV v), σ, [].
  apply (Ectx_prim_step _ _ _ _ _ [] (fill_item Ki (RaiseV v)) (RaiseV v)); eauto.
  by apply H.
Qed.

Lemma reducible_fill K e σ : reducible e σ → reducible (fill K e) σ.
Proof.
  unfold reducible; simpl; rewrite /prim_step'; simpl.
  destruct 1 as [_ [e' [σ' [efs Hstep]]]].
  inversion Hstep. simplify_eq. exists [], (fill (K ++ K0) e2'), σ', efs.
  apply (Ectx_prim_step _ _ _ _ _ (K ++ K0) e1' e2'); eauto.
  by rewrite fill_app.
Qed.

Lemma reducible_fill_item Ki e σ : reducible e σ → reducible (fill_item Ki e) σ.
Proof. by apply (reducible_fill [Ki]). Qed.

Lemma error_irreducible v σ : irreducible (RaiseV v) σ.
Proof.
  unfold irreducible; simpl. unfold prim_step'; simpl.
  intros _ ? ? ?. inversion 1.
  destruct K as [|Ki K].
  - simpl in H0; simplify_eq. by inversion H2.
  - destruct Ki; simpl in H0; by naive_solver.
Qed.

Lemma reducible_not_error e σ : reducible e σ → to_error e = None.
Proof.
  intros ?. case_eq (to_error e);[|done].
  intros ??. specialize (error_irreducible v σ).
  rewrite (of_to_error _ _ H0). by rewrite <-not_reducible.
Qed.

