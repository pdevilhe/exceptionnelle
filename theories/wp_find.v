From iris.proofmode      Require Import base tactics classes.
From iris.base_logic.lib Require Import iprop.
From exceptionnelle      Require Export notation exn_weakestpre weakestpre.

Section wp_find.

Context `{!exnG Σ} `{StateInterp Σ}.

Context (iter : val).
Context (isList : val → list val → iProp Σ).
Context (γ : gname).

Context (iter_spec :
 ∀ (* The loop invariant: *)
      (I : list val → iProp Σ)
    (* The state in case of an error: *)
      (J : val → iProp Σ)
    (l : val ) (xs : list val) (f : val),

      (* The specification for the function being iterated: *)
      ((∀ ys y, ⌜ (ys ++ [y]) `prefix_of` xs ⌝ →
         □ (isList l xs -∗ I  ys         -∗ exn_frag γ J -∗
            WP (f y) ; γ {{ (λ _,
            isList l xs  ∗ I (ys ++ [y])  ∗ exn_frag γ J) }} ))

    -∗

    (* Finally, the specification for the 'iter' function: *)
    □ (isList l xs -∗ I [] -∗ exn_frag γ J -∗
       WP iter f l ; γ {{ λ _,
       isList l xs  ∗ I xs  ∗ exn_frag γ J }})
)%I).

Context (pred : val).
Context (P : val → bool).
Context (pred_spec :
  ∀ (Ψ : val → iProp Σ) (x : val),
     (□ (exn_frag γ Ψ -∗ WP pred x ; γ {{ λ b, ⌜ b = #(P x) ⌝ ∗ exn_frag γ Ψ }}))%I).

Definition find : val := λ: "pred" "xs",
  try:
    iter (λ: "x", if: "pred" "x" then raise "x" else #()) "xs";; NONE
  with
    "x" => SOME "x"
  end.

Lemma find_spec l xs Ψ :
  (□ (isList l xs -∗ exn_frag γ Ψ -∗
      WP find pred l ; γ
      {{ λ v, isList l xs ∗ exn_frag γ Ψ ∗
        match v with
        | NONEV   => ⌜ Forall (not ∘ P) xs ⌝
        | SOMEV y => ∃ ys, ⌜ (ys ++ [y]) `prefix_of` xs ⌝ ∗
                        ⌜ is_true (P y) ⌝ ∗ 
                     ⌜ Forall (not ∘ P) ys ⌝
        | _       => False
        end
      }}))%I.
Proof.
  iModIntro. iIntros "Hl Hfrag". unfold find.
  iApply (Ectxi_wp_bind (AppLCtx _)). reflexivity.
  iApply wp_pure_step. apply pure_prim_step_beta. simpl.
  iApply wp_pure_step. apply pure_prim_step_rec.
  iApply wp_value.
  iApply wp_pure_step. apply pure_prim_step_beta. simpl.
  iApply wp_mono; [|
  iApply (wp_try_with (λ v, isList l xs ∗
    match v with
    | NONEV   => ⌜ Forall (not ∘ P) xs ⌝
    | SOMEV y => ∃ ys, ⌜ (ys ++ [y]) `prefix_of` xs ⌝ ∗
                    ⌜ is_true (P y) ⌝ ∗ 
                 ⌜ Forall (not ∘ P) ys ⌝
    | _       => False
    end)%I (λ y, isList l xs ∗
      ∃ ys, ⌜ (ys ++ [y]) `prefix_of` xs ⌝ ∗
            ⌜ is_true (P y) ⌝ ∗ 
            ⌜ Forall (not ∘ P) ys ⌝)%I
    with "[Hl] [] [Hfrag]"); last by iApply "Hfrag" ].
  by iIntros (v) "[[$ $] $]".
  - iIntros "Hfrag".
    iApply (Ectxi_wp_bind (AppRCtx _)). reflexivity.
    iApply (wp_bind [(AppLCtx _); (AppRCtx _)]). reflexivity.
    iApply wp_pure_step. apply pure_prim_step_rec.
    iApply wp_value. simpl.
    iApply wp_mono; [|
    iApply (iter_spec (λ ys, ⌜ Forall (not ∘ P) ys ⌝)%I with "[] [Hl] [] [Hfrag]") ];
    try by iFrame.
    + iIntros (v) "(Hl & % & Hfrag)". rename H0 into Forall_not.
      iApply (Ectxi_wp_bind (AppLCtx _)). reflexivity.
      iApply wp_pure_step. apply pure_prim_step_rec.
      iApply wp_value.
      iApply wp_pure_step. apply pure_prim_step_beta. simpl.
      iApply wp_pure_step. apply pure_prim_step_InjL.
      iApply wp_value.
      by iFrame.
    + iIntros (ys y xs_prefix). iModIntro.
      iIntros "Hl" (Forall_not) "Hfrag".
      iApply wp_pure_step. apply pure_prim_step_beta. simpl.
      iApply (Ectxi_wp_bind (IfCtx _ _)). reflexivity.
      iPoseProof (pred_spec _ y with "Hfrag") as "pred_spec".
      iApply (wp_strong_mono with "pred_spec [Hl]").
      iIntros (b) "[-> Hfrag]". simpl.
      case_eq (P y); [ intro P_true | intro P_false ].
      * iApply wp_pure_step. apply pure_prim_step_if_true.
        iApply (wp_raise with "[Hl] Hfrag"). by auto.
      * iApply wp_pure_step. apply pure_prim_step_if_false.
        iApply wp_value. iModIntro. iFrame. iPureIntro.
        apply Forall_app; split; [| apply Forall_cons; split; [| apply Forall_nil ]]; auto.
        simpl; rewrite P_false. by intro.
  - iIntros (y) "Hfrag [Hl %]". rename H0 into Post.
    iApply (Ectxi_wp_bind (AppLCtx _)). reflexivity.
    iApply wp_pure_step. apply pure_prim_step_rec.
    iApply wp_value.
    iApply wp_pure_step. apply pure_prim_step_beta. simpl.
    iApply wp_pure_step. apply pure_prim_step_InjR.
    iApply wp_value.
    by iFrame.
Qed.

End wp_find.
