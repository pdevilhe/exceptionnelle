# Exceptionnelle - An Exceptional Program Logic

In this repository, we develop a program logic for a simple language with support for exceptions. The logic is built on top of Iris.

## Contents

 - [theories/lang.v](theories/lang.v): Definition of the `exn_lang` programming language.

 - [theories/exn_weakestpre.v](theories/exn_weakestpre.v): Definition of `EWP e {{ Φ }} {{ Ψ }}`, the exceptional weakest precondition.

 - [theories/weakestpre.v](theories/weakestpre.v): Definition of `WP e ; γ {{ Φ }}`, the traditional weakest precondition with a single postcondition.

## Dependencies

The project is known to compile with:

 - Coq 8.11

 - [std++](https://gitlab.mpi-sws.org/robbertkrebbers/coq-stdpp) version dev.2020-03-10.2.8db97649

 - [Iris](https://gitlab.mpi-sws.org/iris/iris) version dev.2020-03-10.6.79f576aa
